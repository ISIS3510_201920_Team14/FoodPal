//
//  AuthenticationViewController.swift
//  Foodpal
//
//  Created by James Lake  on 13/10/19.
//  Copyright © 2019 team14. All rights reserved.
//

import UIKit
import FirebaseAuth

class AuthenticationViewController: UIViewController {

    
    @IBOutlet weak var emailTxt: UITextField!
    
    @IBOutlet weak var passwordTxt: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("I've loaded")
        self.hideKeyboardWhenTappedAround()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    @IBAction func loginTapped(_ sender: UIButton) {
        let email = emailTxt.text as! String
        let password = passwordTxt.text as! String
        if(email != "" && password != ""){
            Auth.auth().signIn(withEmail: email, password: password){
                [weak self] authResult, error in
                guard let strongSelf = self else {return}
                if (authResult != nil) {
                    //Exit view
                    self?.dismiss(animated: true, completion: nil)
                    print(authResult?.user)
                    print(authResult)
                }
                else{
                    print(error?.localizedDescription)
                    let alert = UIAlertController(title: "Error", message:"¡Parece que la información no es correcta! Inténtalo de nuevo.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{_ in}))
                    self!.present(alert, animated: true, completion: nil)
                }
            }
        }
        else{
            print("Algún campo está vacío: ")
            let alert = UIAlertController(title: "Error", message:"¡Parece que dejaste campos vacíos!", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{_ in}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func editEmail(_ sender: UITextField) {
        let str = emailTxt.text as! String
        if(str.count > 30){
            emailTxt.text = String(str.dropLast())
        }
        let validString = NSCharacterSet(charactersIn: " !#$%^&*()+{}[]|\"<>,~`/:;?-=\\¥'£•¢")

        if let range = str.rangeOfCharacter(from: validString as CharacterSet){
            print(range)
            emailTxt.text = String(str.dropLast())
        }
    }
    
    @IBAction func editPass(_ sender: UITextField) {
        let str = passwordTxt.text as! String
        if(str.count > 30){
            passwordTxt.text = String(str.dropLast())
        }
        let validString = NSCharacterSet(charactersIn: "^&()+{}[]|\"<>,~`/:;-=\\¥'£•¢")

        if let range = str.rangeOfCharacter(from: validString as CharacterSet){
            print(range)
            passwordTxt.text = String(str.dropLast())
        }
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// Put this piece of code anywhere you like
extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
