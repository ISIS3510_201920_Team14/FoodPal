//
//  CameraView.swift
//  Foodpal
//
//  Created by James Lake  on 21/10/19.
//  Copyright © 2019 James Lake . All rights reserved.
//

import Foundation
import UIKit
 
class CameraView: UIView{
    
    
}

extension UIView {
    func hideToasties() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissToast))
        tap.cancelsTouchesInView = false
        self.addGestureRecognizer(tap)
}

    @objc func dismissToast() {
        UIView.animate(withDuration: 0.5, delay: 0.5, options: .curveEaseOut, animations: {
            self.alpha = 0.0
        })
    }
}
