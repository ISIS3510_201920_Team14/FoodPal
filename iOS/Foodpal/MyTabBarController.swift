//
//  TabBarController.swift
//  Foodpal
//
//  Created by James Lake  on 13/11/19.
//  Copyright © 2019 James Lake . All rights reserved.
//

import UIKit

class MyTabBarController: UITabBarController {

    
    private var myTabBarItem = UITabBarItem()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.darkGray], for: .normal)
        let selectedImage1 = UIImage(named: "house.fill")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage1 = UIImage(named:"house")?.withRenderingMode(.alwaysOriginal)
        myTabBarItem = self.tabBar.items![0]
        myTabBarItem.image = deSelectedImage1
        myTabBarItem.selectedImage = selectedImage1
        
        let selectedImage2 = UIImage(named: "camera.fill")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage2 = UIImage(named:"camera")?.withRenderingMode(.alwaysOriginal)
        myTabBarItem = self.tabBar.items![0]
        myTabBarItem.image = deSelectedImage2
        myTabBarItem.selectedImage = selectedImage2
        
        let selectedImage3 = UIImage(named: "person.fill")?.withRenderingMode(.alwaysOriginal)
        let deSelectedImage3 = UIImage(named:"person")?.withRenderingMode(.alwaysOriginal)
        myTabBarItem = self.tabBar.items![0]
        myTabBarItem.image = deSelectedImage3
        myTabBarItem.selectedImage = selectedImage3
        
        self.selectedIndex = 0
        // Do any additional setup after loading the view.
    }
    
   

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
