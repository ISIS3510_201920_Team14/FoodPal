//
//  ProfilesViewController.swift
//  Foodpal
//
//  Created by James Lake  on 13/10/19.
//  Copyright © 2019 team14. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase


class ProfilesViewController: UIViewController {

    var handle: AuthStateDidChangeListenerHandle?
    var userEmail: String?
    
    let db = Firestore.firestore();
    
    @IBOutlet weak var username: UILabel!
    
    @IBOutlet weak var budget: UILabel!
    
    @IBOutlet weak var members: UILabel!
    
    @IBOutlet weak var lastShop: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            print(auth)
            print(user?.email)
            self.userEmail = user?.email
        }
        
        let usersRef = db.collection("users").document(userEmail!)
        
        username.text = String("email")
        
        usersRef.getDocument { (document, error) in
            if let document = document, document.exists {
                let dataDescription = document.data().map(String.init(describing:)) ?? "nil"
                
                
            } else {
                print("Document does not exist")
            }
        }
        
    }
    
    //Delete Handle Listeners
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

