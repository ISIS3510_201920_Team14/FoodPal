//
//  RegisterViewController.swift
//  Foodpal
//
//  Created by James Lake  on 13/10/19.
//  Copyright © 2019 team14. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class RegisterViewController: UIViewController {
    
    let db = Firestore.firestore();
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var passwordVeriTxt: UITextField!

    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func didCreateAccount(_ sender: UIButton) {
        self.registerButton.isEnabled = false
        let email = emailTxt.text as! String
        let password = passwordTxt.text as! String
        let verify = passwordVeriTxt.text as! String
        if(email != "" || !verifyEmail(email: email) ){
            if(password.count >= 6){
                if(password == verify && verify != ""){
                    Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                    }
                    let ref = db.collection("users").addDocument(data: [
                    "email":email,
                    "estrato": 0,
                    "goal":"",
                    "members":[],
                    "preferences":[
                        "likes":[],
                        "dislikes":[]
                    ],
                    "recipes":[],
                    "shoppingList":[]
                    
                    ]){err in
                        if let err = err{
                            print("Error adding Document: \(err)")
                        } else {
                            print ("Document added")
                            let alert = UIAlertController(title: "Sucess!", message:"Has creado una cuenta con tu correo \(email)", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{_ in}))
                            self.present(alert, animated: true, completion: nil)
                            self.registerButton.isEnabled=true
                            self.emailTxt.text = ""
                            self.passwordTxt.text = ""
                            self.passwordVeriTxt.text = ""
                            self.dismiss(animated: true, completion: {
                                return
                            })
                        }
                    }
                }
                else{
                    print("Algún campo está vacío")
                    let alert = UIAlertController(title: "Error", message:"Parece que tus contraseñas no son iguales, o están vacías.", preferredStyle: UIAlertController.Style.alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{_ in}))
                    self.present(alert, animated: true, completion: nil)
                    self.registerButton.isEnabled=true
                }
            }
            else{
                print("La contraseña debe tener al menos 6 caracteres")
                let alert = UIAlertController(title: "Error", message:"La contraseña debe tener al menos 6 caracteres.", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{_ in}))
                self.present(alert, animated: true, completion: nil)
                self.registerButton.isEnabled=true
            }
        }
        else{
            print("Algún campo está vacío: ")
            let alert = UIAlertController(title: "Error", message:"¡Parece que dejaste campos vacíos", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{_ in}))
            self.present(alert, animated: true, completion: nil)
            self.registerButton.isEnabled=true
        }
    }
    
    @IBAction func emailEdit(_ sender: UITextField) {
        let str = emailTxt.text as! String
        if(str.count > 30){
            emailTxt.text = String(str.dropLast())
        }
        let validString = NSCharacterSet(charactersIn: " !#$%^&*()+{}[]|\"<>,~`/:;?-=\\¥'£•¢")

        if let range = str.rangeOfCharacter(from: validString as CharacterSet){
            print(range)
            emailTxt.text = String(str.dropLast())
        }
    }
    
    @IBAction func emailFin(_ sender: UITextField) {
        let str = emailTxt.text as! String
        if(!verifyEmail(email: str)){
            print("Email Inválido")
            let alert = UIAlertController(title: "Error", message:"Parece ser que su email es inválido.", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler:{_ in}))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func passwordEdit(_ sender: UITextField) {
        let str = passwordTxt.text as! String
        if(str.count > 20){
            passwordTxt.text = String(str.dropLast())
        }
        let validString = NSCharacterSet(charactersIn: " ^{}[]|\",~`/;-=\\¥'£•¢")

        if let range = str.rangeOfCharacter(from: validString as CharacterSet){
            print(range)
            passwordTxt.text = String(str.dropLast())
        }
    }
    
    @IBAction func passTwoEdit(_ sender: UITextField) {
        let str = passwordVeriTxt.text as! String
        
        if(str.count > 20){
            passwordVeriTxt.text = String(str.dropLast())
        }
        let validString = NSCharacterSet(charactersIn: " ^{}[]|\",~`/;-=\\¥'£•¢")

        if let range = str.rangeOfCharacter(from: validString as CharacterSet){
            print(range)
            passwordVeriTxt.text = String(str.dropLast())
        }
    }
    
    
    func verifyEmail(email: String) -> BooleanLiteralType{
        return (email.contains("@") && (email.hasSuffix(".com") || email.hasSuffix(".co") || email.hasSuffix(".edu") || email.hasSuffix(".org") || email.hasSuffix(".net")))
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
