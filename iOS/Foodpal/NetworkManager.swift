//
//  NetworkManager.swift
//  Foodpal
//
//  Created by James Lake  on 21/10/19.
//  Copyright © 2019 James Lake . All rights reserved.
//

import Foundation
import Reachability

class NetworkManager: NSObject{
    private var reachability: Reachability!
    
    static let sharedInstance = NetworkManager()
    
    func observeReach(){
       do{
        try self.reachability = Reachability()
            print("Pass1")
        }
        catch{
            print("Unknown error")
        }
        NotificationCenter.default.addObserver(self, selector:#selector(self.reachabilityChanged), name: NSNotification.Name.reachabilityChanged, object: nil)
        do {
            try self.reachability.startNotifier()
            print("Connection Status: \(reachability.connection)")
            print("Pass2")
        }
        catch(let error) {
            print("Error occured while starting reachability notifications : \(error.localizedDescription)")
        }
    }
    
    @objc func reachabilityChanged(note: Notification) {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
        while let presentedViewController = topController.presentedViewController {
            topController = presentedViewController
        }
      let reachability = note.object as! Reachability
      switch reachability.connection {
      case .cellular:
          print("Network available via Cellular Data.")
          Toast.show(message: "Conectado mediante datos.\n Haz click aquí para seguir.", controller: topController)
          break
      case .wifi:
          print("Network available via WiFi.")
          Toast.show(message: "Conectado mediante Wi-Fi.\n Haz click aquí para seguir.", controller: topController)
          break
      case .none:
          print("Network is not available.")
          Toast.show(message: "Red no disponible", controller: topController)
          break
      case .unavailable:
        print("Network is unavailable.")
        Toast.show(message: "Red no disponible. \n Haz click aquí para seguir.", controller: topController)
        break
        }
    }
    
}
    
}
