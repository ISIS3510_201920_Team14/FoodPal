//
//  Scanner.swift
//  Foodpal
//
//  Created by James Lake  on 21/10/19.
//  Copyright © 2019 James Lake . All rights reserved.
//

import Foundation
import AVFoundation
class Scanner: NSObject{
    private var viewController: BarcodeViewController?
    private var captureSession: AVCaptureSession?
    private var codeOutputHandler: (_ code: String) -> Void
    
    private func createCaptureSession() -> AVCaptureSession? {
        let captureSession = AVCaptureSession()
        guard let captureDevice = AVCaptureDevice.default(for: .video) else{
            return nil
        }
        do{
            //Capture Session
            let deviceInput = try AVCaptureDeviceInput(device: captureDevice)
            let metadataOutput = AVCaptureMetadataOutput()
            if(captureSession.canAddInput(deviceInput)){
                captureSession.addInput(deviceInput)
                
            }else{
                return nil
            }
            if(captureSession.canAddOutput(metadataOutput)){
                captureSession.addOutput(metadataOutput)
                if let viewController = self.viewController as? AVCaptureMetadataOutputObjectsDelegate{
                    metadataOutput.setMetadataObjectsDelegate(viewController, queue: DispatchQueue.main)
                    metadataOutput.metadataObjectTypes = self.metaObjectTypes()
                }
            }else{
                return nil
            }
            
        }
        catch{
            return nil
        }
        return captureSession;
    }
    
    private func metaObjectTypes() -> [AVMetadataObject.ObjectType]{
        return [.qr, .code128, .code39, .code39Mod43, .ean13, .ean8, .code93, .interleaved2of5, .pdf417]
    }
    
    private func createPreviewLayer(withCaptureSession captureSession: AVCaptureSession, view: CameraView) -> AVCaptureVideoPreviewLayer{
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        
        return previewLayer
    }
    
    init(withViewController viewController: BarcodeViewController, view: CameraView, codeOutputHandler: @escaping (String) -> Void){
        self.viewController = viewController
        self.codeOutputHandler = codeOutputHandler
        super.init()
        if let captureSession = self.createCaptureSession(){
            self.captureSession = captureSession
            let previewLayer = self.createPreviewLayer(withCaptureSession: captureSession, view: view)
            view.layer.addSublayer(previewLayer)
        }
    }
    
    func requestCaptureSessionStartRunning(){
        guard let captureSession = self.captureSession else {
            return
        }
        if !captureSession.isRunning{
            captureSession.startRunning()
        }
        else{
            captureSession.stopRunning()
        }
    }
    
    func requestCaptureSessionStopRunning(){
        guard let captureSession = self.captureSession else {
            return
        }
        if !captureSession.isRunning{
            captureSession.stopRunning()
        }
    }
    
    func scannerDelegate(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        self.requestCaptureSessionStopRunning()
        if let metadataObject = metadataObjects.first{
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else{
                return
            }
            guard let stringValue = readableObject.stringValue else{
                return
            }
            self.codeOutputHandler(stringValue)
        }
    }
}
