//
//  BarcodeViewController.swift
//  Foodpal
//
//  Created by James Lake  on 21/10/19.
//  Copyright © 2019 James Lake . All rights reserved.
//

import UIKit
import AVFoundation


class BarcodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {


    @IBOutlet weak var barcodeView: CameraView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var priceLabel: UILabel!
    
    private var scanner: Scanner?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scanner = Scanner(withViewController: self, view: barcodeView, codeOutputHandler: self.handleScanner)

        // Do any additional setup after loading the view.
    }
    
    func handleScanner(code: String){
        print(code)
        nameLabel.text=code
    }
    
    
    @IBAction func startOrStop(_ sender: UIButton) {
        if let scanner = self.scanner{
            scanner.requestCaptureSessionStartRunning()
        }
    }
    
    
    @IBAction func showNearMarkets(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "marketsSB") as! GenerateListViewController
        self.present(newViewController, animated: true, completion: nil)
    }
    
    public func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        scanner?.scannerDelegate(output, didOutput: metadataObjects, from: connection)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
