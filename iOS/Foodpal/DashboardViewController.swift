//
//  DashboardViewController.swift
//  Foodpal
//
//  Created by James Lake  on 15/10/19.
//  Copyright © 2019 James Lake . All rights reserved.
//

import UIKit
import Firebase

class DashboardViewController: UIViewController {

    var handle: AuthStateDidChangeListenerHandle?
    
    var userEmail: String?
    
    @IBOutlet weak var planBtn: UIButton!
    
    @IBOutlet weak var tipsBtn: UIButton!
    
    @IBOutlet weak var recipeBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("hello!")
        self.hideKeyboardWhenTappedAround()

        // Do any additional setup after loading the view.
    }
    
    //Set Handle listeners
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        handle = Auth.auth().addStateDidChangeListener { (auth, user) in
            print(auth)
            print(user?.email)
            self.userEmail = user?.email
        }
    }
    
    //Delete Handle Listeners
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Auth.auth().removeStateDidChangeListener(handle!)
    }
    
    
    @IBAction func planningRedirect(_ sender: UIButton) {
        if(userEmail != nil){
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "planningSB") as! GenerateListViewController
            self.present(newViewController, animated: true, completion: nil)
        }
        else{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "authSB") as! AuthenticationViewController
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    

    @IBAction func recipesRedirect(_ sender: UIButton) {
        if(userEmail != nil){
           // let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            //let newViewController = storyBoard.instantiateViewController(withIdentifier: "recipeSB") as! RecipesViewController
            //self.present(newViewController, animated: true, completion: nil)
        }
        else{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "authSB") as! AuthenticationViewController
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    
    @IBAction func tipsRedirect(_ sender: UIButton) {
        if(userEmail != nil){
            //Enviar a la persona al view controller de TIPS que no está creado aun.
        }
        else{
            let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let newViewController = storyBoard.instantiateViewController(withIdentifier: "authSB") as! AuthenticationViewController
            self.present(newViewController, animated: true, completion: nil)
        }
    }
    
    @IBAction func logOut(_ sender: UIButton) {
        let fbAuth = Auth.auth();
        print(userEmail);
        if(userEmail != nil){
            do {
                try fbAuth.signOut()
                print("helo baby")
                let alert = UIAlertController(title: "Logged Out", message:"Has cerrado sesión", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler:{_ in}))
                self.present(alert, animated: true, completion: nil)
            } catch let signOutError as NSError {
                print ("Error signing out: %@", signOutError)
                let alert = UIAlertController(title: "¡Woops!", message:"Parece que no has iniciado sesión", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Close", style: UIAlertAction.Style.default, handler:{_ in}))
                self.present(alert, animated: true, completion: nil)
        }
    }
    }
    @IBAction func moveToBarcode(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "cameraSB") as! BarcodeViewController
        self.present(newViewController, animated: true, completion: nil)
    }
    /*
    // MARK: - Navigation
     
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
