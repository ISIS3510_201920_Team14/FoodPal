var express = require('express');
var router = express.Router();
const db = require('../models/');

/* GET all products. */
router.get('/', function(req, res) {
  db.products.findAll()
  .then((products)=>res.send(products))
  .catch((err)=>{
    console.log('There was an error querrying the products', JSON.stringify(err))
    res.send(err)
  });
});

/* Get one product */
router.get('/:id', function(req, res){
  let id = parseInt(req.params.id);
  db.products.findByPk(id)
  .then((product)=>res.send(product))
  .catch((err)=>{
    console.log('There was an error querrying the product', JSON.stringify(err))
    res.send(err);
  })
});

/* Get one product by name */
router.get('/name/:productName',function(req, res){
  let productName=req.params.productName;
  db.products.findAll({attributes:['product_ID','codebar','name','cost','calories','category_ID'],where: {name: productName}})
    .then((product) => res.send(product))
    .catch((err) => {
      console.log('There was an error querying the product', JSON.stringify(err))
    });
});

router.get('/codebar/:codebarr',function(req, res){
  let codebarr=req.params.codebarr;
  db.products.findAll({attributes:['product_ID','codebar','name','cost','calories','category_ID'],where: {codebar: codebarr}})
    .then((product) => res.send(product))
    .catch((err) => {
      console.log('There was an error querying the product', JSON.stringify(err))
    });
});


/* Post Products */
router.post('/', function(req, res){
  let productAdd = {
    'codebar':req.body.codebar,
    'name':req.body.name,
    'cost':req.body.cost,
    'calories':req.body.calories,
    'category_ID':req.body.category_ID,
    'isCarne':req.body.isCarne,
    'fundamental':req.body.fundamental
  }
  db.products.create(productAdd)
  .then((product)=>res.send(product))
  .catch((err)=>{
    console.log('*** Error Creating Product ***')
    res.status(400).send(err);
  });
});

/* Edits a Product */
router.patch('/:id', function(req, res){
  let id = parseInt(req.params.id);
  db.products.findByPk(id)
  .then((product)=>{
    if(product!=null){
      product.dataValues.product_ID=id;
      product._previousDataValues.product_ID=id;
      product.update({'product_ID':id, 'name':req.body.name, 'cost':req.body.cost,'calories':req.body.calories,'category_ID':req.body.category_ID, 'isCarne':req.body.isCarne,'fundamental':req.body.fundamental})
      .then(()=> res.send(product))
      .catch((err)=>{
        console.log('** Error Updating Product ***', JSON.stringify(err))
        res.status(400).send(err)
      })
    }
    else{
      res.status(400).send("Product wasn't found in the database.")
    }
  })
})

/* Deletes a product */
router.delete('/:id', function(req, res){
  let id = parseInt(re.params.id)
  db.products.findByPk(id)
  .then((product)=> product.destroy())
  .then(()=> res.send({product}))
  .catch((err)=>{
    console.log('*** Error Deleting Product ***', JSON.stringify(err))
    res.status(400).send(err)
  })
})

module.exports = router;