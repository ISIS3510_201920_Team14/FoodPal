var express = require('express');
var router = express.Router();
const db = require('../models/');

/* GET all categories. */
router.get('/', function(req, res) {
  db.categories.findAll()
  .then((categories)=>res.send(categories))
  .catch((err)=>{
    console.log('There was an error querrying the categories', JSON.stringify(err))
    res.send(err)
  });
});

/* Get all items from acategory */
router.get('/:categoryName', function(req, res){
  let name = parseInt(req.params.categoryName);
  db.sequelize.query(`select products.name, products.cost, products.calories, products.isVegetarian, products.inSeason from products, categories where products.category_ID = categories.category_ID and categories.name ="${name}`)
  .then((products)=>res.send(products))
  .catch((err)=>{
      console.log('*** Error fetching all products for the category ***')
      res.status(400).send(err)
  })
});

/* Post a category */
router.post('/', function(req, res){
  let categoryAdd = {
    'name':req.body.name,
    'description':req.body.description
  }
  db.categories.create(categoryAdd)
  .then((category)=>res.send(category))
  .catch((err)=>{
    console.log('*** Error Creating category ***')
    res.status(400).send(err);
  });
});

/* Edits a category */
router.patch('/:id', function(req, res){
  let id = parseInt(req.params.id);
  db.categories.findByPk(id)
  .then((category)=>{
    if(category!=null){
      category.dataValues.category_ID=id;
      category._previousDataValues.category_ID=id;
      category.update({'category_ID':id, 'name':req.body.name, 'description':req.body.description})
      .then(()=> res.send(category))
      .catch((err)=>{
        console.log('** Error Updating category ***', JSON.stringify(err))
        res.status(400).send(err)
      })
    }
    else{
      res.status(400).send("category wasn't found in the database.")
    }
  })
})

module.exports = router;