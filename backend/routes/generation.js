var express = require('express');
var router = express.Router();
const db = require('../models/');

/* Generate a list of suggestions*/
router.post('/', function(req, res, next) {
    console.log(req.body);
    //ARREGLOS DE STRINGS
    let liked = req.body.liked;
    let disliked = req.body.disliked;
    /*
    BAJAR_PESO
    AUMENTAR_PESO
    MANTENER_PESO
    NONE
    VEGETARIANO
    */
    let objectives = req.body.objective;
    //Integer
    let budget = req.body.budget;
    //let members = objectives.length;
 

    let fundamentalProducts = []
    let vegetableProducts = []
    let vegetarianProducts = []
    let proteinProducts = []
    let carbsProducts = []
    let fruitProducts = []
    let otherProducts = []
    let answer = []
    let likedProducts = []

    db.products.findAll().then((products)=>{
        products.sort(function(a,b){
            return(a.cost-b.cost)
        })
        for(p in products){
            //Elimina productos que le disgustan y gustan.
            if(liked.includes(products[p].name)){
                likedProducts.push(products[p]) 
                products.splice(p)
                continue;
            }
            if(disliked.includes(products[p].name)){
                products.splice(p)
                continue;
            }

            //SORTING
            if(products[p].category==5)
                fruitProducts.push(products[p])
            else if(products[p].category==2)
                proteinProducts.push(products[p])
            else if(products[p].category==3)
                carbsProducts.push(products[p])
            else if(products[p].category==4)
                vegetableProducts.push(products[p])
            else
                otherProducts.push(products[p])

            if(!products[p].isCarne)
                vegetarianProducts.push(products[p])

            if(products[p].fundamental)
                fundamentalProducts.push(products[p])
        }
        console.log(objectives)
        switch(objectives){
            case "BAJAR_PESO":
                //Objetivo: Menor calorias, precio irrelevante.
                var maxProdProteina = 8
                var maxProdCarbs = 5
                var maxProdVegetables = 20
                var maxProdFruit = 10
                var maxProducts = 40
                //Mirar productos fundamentales. Agregarlos todos
                for(fun of fundamentalProducts){
                    if(budget-fun.cost < 0){
                        break;
                    }
                    else{
                        answer.push(fun)
                        budget-=fun.cost
                        if(fun.category==2)
                            maxProdProteina--
                        else if(fun.category==3)
                            maxProdCarbs--
                        else if(fun.category==4)
                            maxProdVegetables--
                        else if(fun.category==5)
                            maxProdFruit--
                    }
                }
                //Productos que le gustan
                for(lik of likedProducts){
                    if(budget-lik.cost < 0){
                        break;
                    }
                    else{
                        answer.push(lik)
                        budget-=lik.cost
                        if(lik.category==2)
                            maxProdProteina--
                        else if(lik.category==3)
                            maxProdCarbs--
                        else if(lik.category==4)
                            maxProdVegetables--
                        else if(lik.category==5)
                            maxProdFruit--
                    }
                }
                //Proteinas
                for(prot of proteinProducts){
                    if(budget-prot.cost < 0){
                        break;
                    }
                    if(maxProdProteina <=0)
                        break;
                    else{
                        answer.push(prot)
                        budget-=prot.cost
                        maxProdProteina--
                    }
                }
                //Vegetables
                for(veggie of vegetableProducts){
                    if(budget-veggie.cost < 0){
                        break;
                    }
                    if(maxProdVegetables <=0)
                        break;
                    else{
                        answer.push(veggie)
                        budget-=veggie.cost
                        maxProdVegetables--
                    }
                }
                //Carbs
                for(carb of carbsProducts){
                    if(budget-carb.cost < 0){
                        break;
                    }
                    if(maxProdCarbs <=0)
                        break;
                    else{
                        answer.push(carb)
                        budget-=carb.cost
                        maxProdCarbs--
                    }
                }
                //Fruit
                for(fruit of fruitProducts){
                    if(budget-fruit.cost < 0){
                        break;
                    }
                    if(maxProdFruit <=0)
                        break;
                    else{
                        answer.push(fruit)
                        budget-=fruit.cost
                        maxProdFruit--
                    }
                }
                //Mirar cualquier categoría productos, y agregar los más baratos.
                for(prod of products){
                    if(budget-prod.cost < 0 || answer.length >= maxProducts){
                        break;
                    }
                    else{
                        answer.push(prod)
                        budget-=prod.cost
                    }
                }
                break;
            case "AUMENTAR_PESO":
                //Objetivo: Mayor calorías, precio irrelevante.
                //Objetivo: Menor calorias, precio irrelevante.
                var maxProdProteina = 12
                var maxProdCarbs = 12
                var maxProdVegetables = 16
                var maxProdFruit = 10
                var maxProducts = 50
                //Mirar productos fundamentales. Agregarlos todos
                for(fun of fundamentalProducts){
                    if(budget-fun.cost < 0){
                        break;
                    }
                    else{
                        answer.push(fun)
                        budget-=fun.cost
                        if(fun.category==2)
                            maxProdProteina--
                        else if(fun.category==3)
                            maxProdCarbs--
                        else if(fun.category==4)
                            maxProdVegetables--
                        else if(fun.category==5)
                            maxProdFruit--
                    }
                }
                //Productos que le gustan
                for(lik of likedProducts){
                    if(budget-lik.cost < 0){
                        break;
                    }
                    else{
                        answer.push(lik)
                        budget-=lik.cost
                        if(lik.category==2)
                            maxProdProteina--
                        else if(lik.category==3)
                            maxProdCarbs--
                        else if(lik.category==4)
                            maxProdVegetables--
                        else if(lik.category==5)
                            maxProdFruit--
                    }
                }
                //Proteinas
                for(prot of proteinProducts){
                    if(budget-prot.cost < 0){
                        break;
                    }
                    if(maxProdProteina <=0)
                        break;
                    else{
                        answer.push(prot)
                        budget-=prot.cost
                        maxProdProteina--
                    }
                }
                //Vegetables
                for(veggie of vegetableProducts){
                    if(budget-veggie.cost < 0){
                        break;
                    }
                    if(maxProdVegetables <=0)
                        break;
                    else{
                        answer.push(veggie)
                        budget-=veggie.cost
                        maxProdVegetables--
                    }
                }
                //Carbs
                for(carb of carbsProducts){
                    if(budget-carb.cost < 0){
                        break;
                    }
                    if(maxProdCarbs <=0)
                        break;
                    else{
                        answer.push(carb)
                        budget-=carb.cost
                        maxProdCarbs--
                    }
                }
                //Fruit
                for(fruit of fruitProducts){
                    if(budget-fruit.cost < 0){
                        break;
                    }
                    if(maxProdFruit <=0)
                        break;
                    else{
                        answer.push(fruit)
                        budget-=fruit.cost
                        maxProdFruit--
                    }
                }
                //Mirar cualquier categoría productos, y agregar los más baratos.
                for(prod of products){
                    if(budget-prod.cost < 0 || answer.length >= maxProducts){
                        break;
                    }
                    else{
                        answer.push(prod)
                        budget-=prod.cost
                    }
                }
                break;
            case "MANTENER_PESO":
                //Objetivo: Calorías exactas
                //Mirar productos fundamentales. Agregarlos todos
                for(fun of fundamentalProducts){
                    if(budget-fun.cost < 0){
                        control = true;
                        break;
                    }
                    else{
                        answer.push(fun)
                        budget-=fun.cost
                    }
                }
                //Productos que le gustan
                for(lik of likedProducts){
                    if(budget-lik.cost < 0){
                        control = true;
                        break;
                    }
                    else{
                        answer.push(lik)
                        budget-=lik.cost
                    }
                }
                //Mirar cualquier categoría productos, y agregar los más baratos.
                for(prod of products){
                    if(budget-prod.cost < 0){
                        control = true;
                        break;
                    }
                    else{
                        answer.push(prod)
                        budget-=prod.cost
                    }
                }
                break;
            case "VEGETARIANO":
                //Objetivo. Nada carnívoro, mantener calorías exactas.
                //Mirar productos fundamentales. Agregarlos todos
                for(fun of fundamentalProducts){
                    if(budget-fun.cost < 0){
                        break;
                    }
                    else{
                        answer.push(fun)
                        budget-=fun.cost
                    }
                }
                //Productos que le gustan
                for(lik of likedProducts){
                    if(budget-lik.cost < 0){
                        break;
                    }
                    else{
                        answer.push(lik)
                        budget-=lik.cost
                    }
                }
                //Mirar categoría vegetariana productos, y agregar los más baratos.
                for(prod of vegetarianProducts){
                    if(budget-prod.cost < 0){
                        break;
                    }
                    else{
                        answer.push(prod)
                        budget-=prod.cost
                    }
                }
                break;
            case "NONE":
                //Objetivo: Bang for your buck. Menor precio always.
                //Mirar productos fundamentales. Agregarlos todos
                for(fun of fundamentalProducts){
                    if(budget-fun.cost < 0){
                        break;
                    }
                    else{
                        answer.push(fun)
                        budget-=fun.cost
                    }
                }
                //Productos que le gustan
                for(lik of likedProducts){
                    if(budget-lik.cost < 0){
                        break;
                    }
                    else{
                        answer.push(lik)
                        budget-=lik.cost
                    }
                }
                //Mirar cualquier categoría productos, y agregar los más baratos.
                for(prod of products){
                    if(budget-prod.cost < 0){
                        break;
                    }
                    else{
                        answer.push(prod)
                        budget-=prod.cost
                    }
                }
                break;
            }
            res.send(answer);
    }).catch((err)=>{
        console.log(err)
    });
    console.log("fin");
});

module.exports = router;