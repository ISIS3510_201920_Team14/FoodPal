var express = require('express');
var router = express.Router();

const admin = require('firebase-admin');
let serviceAccount = require('../foodpal-moviles-firebase-adminsdk-5ilsq-232ab479ff.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});


let db = admin.firestore();
var vegetarianos = 0
var looseWeight = 0
var gainWeight = 0
var sameWeight = 0


router.get('/percentagesRecipe', (req, res) => {
  db.collection('recipes').get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        const obj = doc.data()

        if (obj.objective == "VEGETARIANO") {
          vegetarianos++
        }
        else if (obj.objective == "AUMENTAR_PESO") {
          gainWeight++
        }
        else if (obj.objective == "PERDER_PESO") {
          looseWeight++
        }
        else if (obj.objective == "MANTENER_PESO") {
          sameWeight
        }

      });

      var total = vegetarianos + gainWeight + looseWeight + sameWeight
      var resVeg = vegetarianos / total
      var resGain = gainWeight / total
      var resLoose = looseWeight / total
      var resSame = sameWeight / total

      console.log(resVeg)
      console.log(resGain)
      console.log(resLoose)
      console.log(resSame)

      res.send({
        resVeg, resGain, resLoose, resSame

      })
    })
    .catch((err) => {
      console.log('Error getting documents', err);
    });
})

router.get('/likedVeg', (req, res) => {
  attributeByObj('VEGETARIANO', 'likes', (data) => {
    if (data) {
      res.send(data)
    } else {
      res.send([])
    }
  })
})

router.get('/dislikedVeg', (req, res) => {
  attributeByObj('VEGETARIANO', 'dislikes', (data) => {
    if (data) {
      res.send(data)
    } else {
      res.send([])
    }
  })
})

router.get('/likedGW', (req, res) => {
  attributeByObj('AUMENTAR_PESO', 'likes', (data) => {
    if (data) {
      res.send(data)
    } else {
      res.send([])
    }
  })
})

router.get('/dislikedGW', (req, res) => {
  attributeByObj('AUMENTAR_PESO', 'dislikes', (data) => {
    if (data) {
      res.send(data)
    } else {
      res.send([])
    }
  })
})

router.get('/', (req, res) => {
  res.send('Analytics Dashboard')
})

router.get('/averageBudget', (req, res) => {
  var budgetTotal = 0
  var numUsers = 0
  db.collection('users').get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        const obj = doc.data()
        numUsers++
        budgetTotal += obj.budget

      });

      var answ = budgetTotal / numUsers

      res.send({
        answ

      })
    })
    .catch((err) => {
      console.log('Error getting documents', err);
    });
})

router.get('/objectives', (req, res) => {

  db.collection('users')
    .get()
    .then(ss => {
      let c = null
      let map = {}
      let sum = 0

      ss.forEach(d => {
        const data = d.data()
        console.log(data)
        c = data['objective'];

        if (!(c in map)) {
          map[c] = 0
        }
        map[c] = map[c] + 1
        sum++;

        Object.entries(data['members']).forEach(e => {
          sum++;
          const m = e[1]
          c = m['objective']
          if (!(c in map)) {
            map[c] = 0
          }
          map[c] = map[c] + 1
        })
      })
      res.send(Object.entries(map).map(f => {
        const d = {}
        d[f[0]] = `${(f[1] / sum * 100).toFixed(2)}%`
        return d
      }))
    }).catch(er => res.send(er))
})

router.get('/mostBought', (req, res) => {
  db.collection('food')
    .get()
    .then(ss => {
      let most = { product: null, qty: 0 }
      ss.forEach(d => {
        const el = d.data()
        const qb = +el['quantityBought']
        if (qb > most.qty) {
          most = { product: el['name'], qty: qb }
        }
      })
      res.send(most)
    })
})

router.get('/lessBought', (req, res) => {
  db.collection('food')
    .get()
    .then(ss => {
      let less = { product: null, qty: Infinity }

      ss.forEach(d => {
        const el = d.data()
        const qb = +el['quantityBought']
        if (qb < less.qty) {
          less = { product: el['name'], qty: qb }
        }
      })

      res.send(less)
    })
})

router.get('/averageFamily', (req, res) => {
  var totalMiembros = 0
  var familias = 0
  var average = 0
  db.collection('users').get()
    .then((snapshot) => {
      snapshot.forEach((doc) => {
        const obj = doc.data()

        var miembros = Object.keys(obj.members).length

        familias++

        totalMiembros += miembros

      });

      var answw = totalMiembros / familias

      res.send({
        answw

      })
    })
    .catch((err) => {
      console.log('Error getting documents', err);
    });
})

function attributeByObj(obj, attr, cbk) {
  db.collection('users')
    .where('objective', '==', obj)
    .get()
    .then(ss => {
      let mostLiked = {}
      ss.forEach(d => {
        d.data()['preferences'][attr].forEach(f => {
          if (!(f in mostLiked)) {
            mostLiked[f] = 0
          }
          mostLiked[f] = mostLiked[f] + 1
        })
      })
      cbk(Object.entries(mostLiked).sort((b, a) => a[1] - b[1])[0])
    })
}

module.exports = router;
