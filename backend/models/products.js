'use strict';
module.exports = (sequelize, DataTypes) => {
  const products = sequelize.define('products', {
    product_ID: {type:DataTypes.INTEGER, primaryKey:true},
    codebar: DataTypes.INTEGER,
    codebar:DataTypes.INTEGER,
    name: DataTypes.STRING,
    cost: DataTypes.DOUBLE,
    calories: DataTypes.INTEGER,
    category_ID: {type:DataTypes.INTEGER, references: 'categories', referencesKey: 'category_ID' },
    isCarne: DataTypes.BOOLEAN,
    fundamental: DataTypes.BOOLEAN
  }, {timestamps: false});

  return products;
};