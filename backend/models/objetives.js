'use strict';
module.exports = (sequelize, DataTypes) => {
  const objectives = sequelize.define('objectives', {
    objective_ID: {type:DataTypes.INTEGER, primaryKey:true},
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {timestamps: false});

  return objectives;
};