'use strict';
module.exports = (sequelize, DataTypes) => {
  const categories = sequelize.define('categories', {
    category_ID: {type:DataTypes.INTEGER, primaryKey:true},
    name: DataTypes.STRING,
    description: DataTypes.STRING
  }, {timestamps: false});

  return categories;
};