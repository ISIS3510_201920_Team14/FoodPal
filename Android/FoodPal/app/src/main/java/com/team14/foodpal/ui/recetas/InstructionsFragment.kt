package com.team14.foodpal.ui.recetas


import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView

import com.team14.foodpal.R
import kotlinx.android.synthetic.main.fragment_ingredients.*

/**
 * A simple [Fragment] subclass.
 */
class InstructionsFragment (grupo: ArrayList<String>): Fragment() {

    var instrucciones = grupo
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_ingredients, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val linearLayout1 = getView()?.findViewById(R.id.linearIngredientes1) as LinearLayout
        val linearLayout2 = getView()?.findViewById(R.id.linearIngredientes2) as LinearLayout


        Log.d("VIEW",instrucciones.size.toString())

        for (x in 0 until instrucciones.size)
        {
            val tv_dynamic = TextView(this.context)
            val pasos = instrucciones[x]
            tv_dynamic.text=pasos
            tv_dynamic.setBackgroundResource(R.drawable.ingredients_list_left)
            tv_dynamic.textSize=20f
            tv_dynamic.gravity= Gravity.CENTER

            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )

            val layoutParams2 = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )


            layoutParams.weight= 2f

            layoutParams.weight=0f

            layoutParams.setMargins(0, 20, 0, 10)

            tv_dynamic.layoutParams = layoutParams
            linearLayout1.layoutParams = layoutParams
            linearLayout2.layoutParams = layoutParams2


            linearLayout1.addView(tv_dynamic)
            Log.d("INSIDE FRAGMENT", pasos.toString())
        }
    }


}
