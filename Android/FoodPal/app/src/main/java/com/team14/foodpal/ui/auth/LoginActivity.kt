package com.team14.foodpal.ui.auth

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import com.google.android.material.snackbar.Snackbar
import com.team14.foodpal.databinding.ActivityLoginBinding
import com.team14.foodpal.ui.MainActivity
import com.team14.foodpal.util.toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.team14.foodpal.util.NetworkUtil
import kotlinx.android.synthetic.main.activity_choose.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_signup.*


class LoginActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.team14.foodpal.R.layout.activity_login)
        auth = FirebaseAuth.getInstance()


        button4.setOnClickListener{
            button4.isEnabled = false
            doLogin()

        }

    }

    private fun doLogin() {

        val alertDialogBuilder = AlertDialog.Builder(this)

        if(!Patterns.EMAIL_ADDRESS.matcher(editText7.text.toString()).matches()){
            button4.isEnabled = true

            editText7.setError( "El Correo electronico es invalido.")
            editText7.requestFocus()
            return
        }
        if(editText8.text.toString().isNullOrEmpty()){
            button4.isEnabled = true

            editText8.setError( "La contraseña no puede estar vacía.")
            editText8.requestFocus()
            return
        }

        if(NetworkUtil().isNetworkAvailable(this@LoginActivity)) {
            auth.signInWithEmailAndPassword(editText7.text.toString(), editText8.text.toString())
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {

                        val user = auth.currentUser
                        updateUI(user)
                    } else {
                        button4.isEnabled = true
                        alertDialogBuilder.setTitle("Error iniciando sesión.")
                        alertDialogBuilder.setMessage("No se ha podido autenticar el usuario, asegúrate que los datos ingresados son correctos.")
                        alertDialogBuilder.setNeutralButton("Entendido.") { _, _ ->
                            Toast.makeText(
                                applicationContext,
                                "You cancelled the dialog.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        // Finally, make the alert dialog using builder
                        val dialog: AlertDialog = alertDialogBuilder.create()

                        // Display the alert dialog on app interface
                        dialog.show()

                        updateUI(null)
                    }

                    // ...
                }
        }
        else
        {
            val snackbar = Snackbar.make(loginlayout, "No hay conexión a internet!",
                Snackbar.LENGTH_INDEFINITE).setAction("Aceptar", View.OnClickListener {  })
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()
        }

    }
    public override fun onStart()
    {

        super.onStart()
        val user: FirebaseUser? = auth.currentUser
        updateUI(user)
    }

    private fun updateUI(user: FirebaseUser?) {

        if(user != null )
        {
            finishAffinity()
            val intent = Intent(this,MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            intent.putExtra(SessionManager.KEY_EMAIL, user.email)
            startActivity(intent)
            toast("Sesion Iniciada!" )
            finish()
        }

    }


}
