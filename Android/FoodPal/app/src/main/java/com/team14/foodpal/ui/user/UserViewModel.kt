package com.team14.foodpal.ui.user

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.team14.foodpal.util.Member
import com.team14.foodpal.util.RealMember
import com.team14.foodpal.util.User
import java.util.*

class UserViewModel(private val repository: UserRepository) : ViewModel() {
    private val user: MutableLiveData<User> = MutableLiveData()

    fun getUser(): LiveData<User> {
        return user
    }

    fun loadUser(email: String) {
        repository.getUser(email) { user.value = it }
    }

    fun retrieveUser(email: String, cbk: (User?) -> Unit) {
        repository.getUser(email) { cbk(it) }
    }

    fun updateUser(user: Member, email: String, position: String, onSuccess: () -> Unit) {
        if (position == "") {
            repository.updateUser(user as User, onSuccess)
        } else {
            repository.updateMember(user, email, position, onSuccess)
        }
    }

    fun addMember(member: Member, email: String, onSuccess: (member: RealMember) -> Unit) {
        val id = UUID.randomUUID().toString()
        val rMember = RealMember(
            id = id,
            name = member.name,
            objective = member.objective,
            preferences = member.preferences
        )
        repository.addMember(rMember, email, onSuccess)
    }

    fun savePreferences(
        email: String,
        position: String,
        likes: Boolean,
        data: List<String>,
        onSuccess: () -> Unit
    ) {
        val attribute = if (likes) "likes" else "dislikes"
        if (position == "") {
            repository.savePreferencesUser(email, attribute, data, onSuccess)
        } else {
            repository.savePreferencesMember(email, attribute, position, data, onSuccess)
            return
        }
    }

    fun saveBudget(email: String, budget: Int, onError: () -> Unit = {}, onSuccess: () -> Unit) {
        repository.saveBudget(email, budget, onError = {
            onError()
        }) {
            user.value?.budget = budget
            onSuccess()
        }
    }

    fun getFood(onSuccess: (List<String>) -> Unit) {
        repository.getFood(onSuccess)
    }
}
