package com.team14.foodpal.ui.profile

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.android.material.chip.Chip
import com.google.android.material.snackbar.Snackbar
import com.team14.foodpal.R
import com.team14.foodpal.util.Member
import com.team14.foodpal.util.Objectives
import com.team14.foodpal.util.Preferences
import kotlinx.android.synthetic.main.content_edit_profile.view.*
import kotlinx.android.synthetic.main.fragment_edit_profile.view.*


class EditProfileFragment : Fragment() {
    private var editProfileDoneCallback: OnEditProfileDone? = null
    private var addFoodCallback: OnAddFood? = null
    private var member: Member? = null
    private var mail: String? = null
    private var memberPos: String = ""
    private var isAdding: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.apply {
            getSerializable(MEMBER)?.let { member = it as Member }
            getString(EMAIL)?.let { mail = it }
            getString(MEMBER_POS, "").let { memberPos = it }
            getBoolean(ADD_MEMBER, false).let { isAdding = it }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_edit_profile, container, false)

        val types = resources.getStringArray(R.array.types)
        view.spinner_type.adapter = object : ArrayAdapter<String>(
            context!!,
            android.R.layout.simple_spinner_dropdown_item,
            listOf(resources.getString(R.string.type), *types)
        ) {
            override fun isEnabled(position: Int): Boolean = position != 0

            override fun getDropDownView(
                position: Int,
                convertView: View?,
                parent: ViewGroup
            ): View {
                val va = super.getDropDownView(position, convertView, parent)
                val tv = va as TextView
                if (position == 0) {
                    tv.setTextColor(Color.GRAY)
                }
                return va
            }
        }

        populateView(view)

        view.save_profile.setOnClickListener {
            if (isAdding) {
                if (view.editName.text.toString().trim().isEmpty()) {
                    val snackbar = Snackbar.make(
                        view,
                        getString(R.string.new_person_no_name),
                        Snackbar.LENGTH_INDEFINITE
                    ).setAction(getString(R.string.aceptar), View.OnClickListener { })
                    snackbar.setActionTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    editProfileDoneCallback?.newMemberAdded(
                        Member(
                            name = view.editName.text.toString(),
                            objective = Objectives.fromStr(view.spinner_type.selectedItem as String),
                            preferences = Preferences(
                                likes = member?.preferences?.likes ?: listOf(),
                                dislikes = member?.preferences?.dislikes ?: listOf()
                            )
                        ),
                        mail!!
                    )
                }
            } else {
                if (view.editName.text.toString().trim().isEmpty()) {
                    val snackbar = Snackbar.make(
                        view,
                        getString(R.string.update_person_no_name),
                        Snackbar.LENGTH_INDEFINITE
                    ).setAction(getString(R.string.aceptar), View.OnClickListener { })
                    snackbar.setActionTextColor(Color.WHITE)
                    snackbar.show()
                } else {
                    editProfileDoneCallback?.profileUpdated(
                        member!!,
                        Member(
                            name = view.editName.text.toString(),
                            objective = Objectives.fromStr(view.spinner_type.selectedItem as String)
                        ),
                        mail!!,
                        memberPos
                    )
                }
            }
        }

        view.addInclude.setOnClickListener {
            addFoodCallback?.addInclude(getMember(view), memberPos, isAdding)
        }
        view.addExclude.setOnClickListener {
            addFoodCallback?.addExclude(getMember(view), memberPos, isAdding)
        }

        return view
    }

    private fun getMember(view: View): Member {
        val mem = Member(
            name = view.editName.text.toString(),
            objective = Objectives.fromStr(view.spinner_type.selectedItem as String),
            preferences = Preferences(
                likes = member?.preferences?.likes ?: listOf(),
                dislikes = member?.preferences?.dislikes ?: listOf()
            )
        )

        return if (isAdding) mem else member!!
    }

    private fun populateView(view: View) {
        member?.let {
            val (likes, dislikes) = it.preferences

            view.includeMsg.visibility = if (likes.isEmpty()) View.VISIBLE else View.GONE
            view.excludeMsg.visibility = if (dislikes.isEmpty()) View.VISIBLE else View.GONE

            view.editName.setText(it.name)
            view.spinner_type.apply {
                for (i in 0 until this.adapter.count) {
                    if (this.adapter.getItem(i) == it.objective.str) {
                        setSelection(i)
                        break
                    }
                }
            }

            for (i in likes) {
                val chip = Chip(context)
                chip.text = i
                chip.setChipBackgroundColorResource(R.color.colorAccent)
                view.includesContainer.addView(chip)
            }

            for (i in dislikes) {
                val chip = Chip(context)
                chip.text = i

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    chip.setChipBackgroundColorResource(R.color.design_default_color_error)
                    chip.setTextColor(
                        resources.getColor(
                            R.color.design_default_color_on_primary,
                            context!!.theme
                        )
                    )
                }

                view.excludesContainer.addView(chip)
            }
        }

        member = member ?: Member()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnEditProfileDone) {
            editProfileDoneCallback = context
        }
        if (context is OnAddFood) {
            addFoodCallback = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        editProfileDoneCallback = null
        addFoodCallback = null
    }

    interface OnEditProfileDone {
        fun profileUpdated(original: Member, member: Member, email: String, position: String)
        fun newMemberAdded(member: Member, email: String)
    }

    interface OnAddFood {
        fun addInclude(actualMember: Member, position: String, isAdding: Boolean)
        fun addExclude(actualMember: Member, position: String, isAdding: Boolean)
    }

    companion object {
        const val MEMBER = "member"
        const val ADD_MEMBER = "add_member"
        const val EMAIL = "email"
        const val MEMBER_POS = "member_pos"
    }
}
