package com.team14.foodpal.ui.profile.member

import java.io.Serializable


data class Member(
    val name: String  = "",
    val objective: String = "Mantener Peso",
    var likes: List<String> = listOf(),
    var dislikes: List<String> = listOf()
): Serializable {
    override fun toString(): String {
        return this.name
    }
}
