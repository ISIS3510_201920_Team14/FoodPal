package com.team14.foodpal.ui.recetas

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.Toast
import android.widget.ToggleButton
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FieldValue
import com.team14.foodpal.util.NetworkUtil
import com.team14.foodpal.util.toast
import kotlinx.android.synthetic.main.activity_receta.*
import android.content.Context


class RecetaActivity : AppCompatActivity(){

    private lateinit var auth: FirebaseAuth

    var db = FirebaseFirestore.getInstance()


    override fun onCreate(savedInstanceState: Bundle?){


        var group= mapOf<String, String>()

        var group2= arrayListOf<String>()


        val receta = intent.getStringExtra("NAME_RECETA")

        val docRef = db.collection("recipes").document(receta)



        super.onCreate(savedInstanceState)

        setContentView( com.team14.foodpal.R.layout.activity_receta)


        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    group=document. get("ingredients") as Map<String, String>
                    group2=document. get("instrucciones") as ArrayList<String>

                    val viewPager: ViewPager = findViewById(com.team14.foodpal.R.id.viewPager)

                    val adapter = MyViewPagerAdapter(supportFragmentManager)
                    adapter.addFragment(IngredientsFragment(group), "Ingredientes")
                    adapter.addFragment(InstructionsFragment(group2), "Intrucciones")
                    viewPager.adapter = adapter
                    tabs.setupWithViewPager(viewPager)


                    val toggle: ToggleButton = findViewById(com.team14.foodpal.R.id.toggleButton)

                    auth = FirebaseAuth.getInstance()
                    val userActual = auth.currentUser!!.email.toString()

                    val preferences = getPreferences(Context.MODE_PRIVATE)
                    val tgpref = preferences.getBoolean("tgpref", true)


                    val dodo = db.collection("users").document(userActual)
                    dodo.get()
                        .addOnSuccessListener { document ->
                            if (document != null) {
                                toggle.isEnabled = true
                                var recetass = document.get("recipes").toString()

                                if(recetass.contains(receta))
                                {
                                    toggle.setChecked(true)
                                }
                            } else {
                                Log.d("NO HAY USER", "No such document")
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d("ERROR ", "get failed with ", exception)
                        }

                        toggle.setOnCheckedChangeListener { _, isChecked ->
                            if(NetworkUtil().isNetworkAvailable(this)) {
                                toggle.isEnabled = true
                                if (isChecked) {
                                    db.collection("users").document(userActual)
                                        .update("recipes", FieldValue.arrayUnion(receta))


                                } else {
                                    // The toggle is disabled
                                    db.collection("users").document(userActual)
                                        .update("recipes", FieldValue.arrayRemove(receta))

                                }
                            }
                            else
                            {
                                toggle.isEnabled = false
                                val snackbar = Snackbar.make(linearlala, "¡No hay conexión a internet!, no se han guardado los datos.",
                                    Snackbar.LENGTH_INDEFINITE).setAction("Aceptar", View.OnClickListener {  })
                                snackbar.setActionTextColor(Color.WHITE)
                                snackbar.show()
                            }


                        }





                } else {
                    Log.d("NO DOC", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("FAILURE", "get failed with ", exception)
            }



    }



    class MyViewPagerAdapter(manager: FragmentManager) : FragmentPagerAdapter(manager,  FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT){

        private val fragmentList: MutableList<Fragment> = ArrayList()
        private val titleList : MutableList<String> = ArrayList()

        override fun getItem(position: Int): Fragment {
            return fragmentList[position]
        }

        override fun getCount(): Int {
            return fragmentList.size
        }
        fun addFragment(fragment:Fragment, title:String)
        {
            fragmentList.add(fragment)
            titleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return titleList[position]
        }


    }
}