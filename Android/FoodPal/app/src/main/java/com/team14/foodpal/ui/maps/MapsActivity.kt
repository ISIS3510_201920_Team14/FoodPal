package com.team14.foodpal.ui.maps

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.snackbar.Snackbar
import com.team14.foodpal.R
import com.team14.foodpal.util.NetworkUtil
import com.team14.foodpal.util.bitmapDescriptorFromVector
import com.team14.foodpal.util.hasPermissionTo
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory


class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener {

    private lateinit var mMap: GoogleMap
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var lastLocation: Location

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        if (!hasPermissionTo(this, android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                LOCATION_PERMISSION_REQUEST_CODE
            )
            return
        } else {
            onLocation()
        }
        mMap.setInfoWindowAdapter(MarkerWindow(this, R.layout.window_map))
        mMap.setOnInfoWindowClickListener(this)
    }

    override fun onInfoWindowClick(marker: Marker?) {
        marker?.let {
            val uri = "google.navigation:q=${marker.position.str()}"
            val gmmIntentUri = Uri.parse(uri)

            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")

            startActivity(mapIntent)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                val snackbar = Snackbar.make(
                    findViewById(R.id.map),
                    getString(R.string.location_problem),
                    Snackbar.LENGTH_INDEFINITE
                )
                snackbar.setActionTextColor(Color.WHITE)
                snackbar.show()
            } else {
                onLocation()
            }
        }
    }

    private fun getCloseMarkets(coords: LatLng) {
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            val req: Retrofit = Retrofit.Builder()
                .baseUrl("https://maps.googleapis.com/")
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

            val placesApi = req.create(PlacesApi::class.java)
            val call: Call<PlaceResponse> = placesApi.getPlacesAsync(
                location = coords.str(),
                radius = 1000,
                type = "grocery_or_supermarket",
                key = "AIzaSyAHItmCpafAS-e-ii6CqlVdhIhOUFuMBOk"
            )
            call.enqueue(callback())
        } else {
            val snackbar = Snackbar.make(
                findViewById(R.id.map),
                getString(R.string.noInternet),
                Snackbar.LENGTH_INDEFINITE
            )
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()
        }
    }

    private fun callback(): Callback<PlaceResponse> {
        return object : Callback<PlaceResponse> {
            override fun onFailure(call: Call<PlaceResponse>, t: Throwable) {
                val snackbar = Snackbar.make(
                    findViewById(R.id.map),
                    getString(R.string.problem_finding_market),
                    Snackbar.LENGTH_INDEFINITE
                )
                snackbar.setActionTextColor(Color.WHITE)
                snackbar.show()
            }

            override fun onResponse(call: Call<PlaceResponse>, response: Response<PlaceResponse>) {
                if (response.isSuccessful) {
                    val body = response.body()
                    body?.let {
                        if (body.isOk()) {
                            body.results.forEach {
                                val marker = MarkerOptions()
                                    .position(it.getLatLng())
                                    .title(it.name)
                                    .snippet("${it.vicinity}")
                                marker.icon(
                                    bitmapDescriptorFromVector(
                                        applicationContext,
                                        R.drawable.map_icon
                                    )
                                )
                                mMap.addMarker(marker)
                            }
                        } else {
                            Toast.makeText(
                                applicationContext,
                                body.error_message,
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                } else {
                    val snackbar = Snackbar.make(
                        container, response.message(),
                        Snackbar.LENGTH_INDEFINITE
                    ).setAction(getString(R.string.aceptar), View.OnClickListener { })
                    snackbar.setActionTextColor(Color.WHITE)
                    snackbar.show()
                }
            }
        }
    }

    private fun onLocation() {
        mMap.isMyLocationEnabled = true
        fusedLocationClient.lastLocation.addOnSuccessListener(this) {
            it?.let {
                lastLocation = it
                val currentLatLong = LatLng(it.latitude, it.longitude)
                val posZoom = CameraUpdateFactory
                    .newCameraPosition(
                        CameraPosition
                            .builder()
                            .zoom(15f)
                            .target(currentLatLong)
                            .build()
                    )
                mMap.animateCamera(posZoom)
                getCloseMarkets(currentLatLong)
            }
        }
    }

    private fun LatLng.str(): String {
        return "${this.latitude},${this.longitude}"
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }
}
