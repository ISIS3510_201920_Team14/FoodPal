package com.team14.foodpal.ui.tips

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.TextView
import com.google.firebase.firestore.FirebaseFirestore
import com.team14.foodpal.R

class ActivityTips : AppCompatActivity() {

    var db = FirebaseFirestore.getInstance()

    val docRef = db.collection("tips").document("Tips de ahorro")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tips)

        docRef.get()
            .addOnSuccessListener { document ->
                if (document != null) {
                    Log.d("entra", "Entra al IF")
                    var tips: ArrayList<String> = document.get("Tips") as ArrayList<String>
                    val linearLayout1 = findViewById<LinearLayout>(R.id.linearTips)
                    for(x in 0 until tips.size)
                    {
                        val tv_dynamic = TextView(this)
                        tv_dynamic.text = tips[x]
                        tv_dynamic.textSize=20f
                        tv_dynamic.setBackgroundResource(R.drawable.edit_text_round_green_background)
                        tv_dynamic.gravity= Gravity.CENTER
                        val layoutParams = LinearLayout.LayoutParams(
                            LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                        )
                        layoutParams.setMargins(0, 20, 0, 10)

                        tv_dynamic.layoutParams = layoutParams

                        linearLayout1.addView(tv_dynamic)

                    }


                } else {
                    Log.d("NO HAY USER", "No such document")
                }
            }
            .addOnFailureListener { exception ->
                Log.d("ERROR ", "get failed with ", exception)
            }

    }
}
