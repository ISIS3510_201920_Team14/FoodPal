package com.team14.foodpal.ui

import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.navigation.NavArgument
import androidx.navigation.NavType
import androidx.navigation.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.firestore.FirebaseFirestore
import com.team14.foodpal.R
import com.team14.foodpal.ui.auth.SessionManager
import com.team14.foodpal.ui.profile.EditProfileFragment
import com.team14.foodpal.ui.profile.Food
import com.team14.foodpal.ui.profile.FoodFragment
import com.team14.foodpal.ui.profile.ProfileFragment
import com.team14.foodpal.ui.user.UserRepository
import com.team14.foodpal.ui.user.UserViewModel
import com.team14.foodpal.ui.user.UserViewModelFactory
import com.team14.foodpal.util.Member
import com.team14.foodpal.util.NetworkUtil
import com.team14.foodpal.util.User
import kotlinx.android.synthetic.main.activity_main.*
import java.io.Serializable
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T


class MainActivity :
    AppCompatActivity(),
    ProfileFragment.OnNewMember,
    ProfileFragment.OnEditProfile,
    ProfileFragment.OnMemberClick,
    EditProfileFragment.OnAddFood,
    EditProfileFragment.OnEditProfileDone,
    FoodFragment.OnFoodSave,
    ProfileFragment.OnBudgetUpdate,
    ProfileFragment.OnRefreshProfile {

    private var state: Int = 0
    private var user: User? = null
    private var userEmail: String? = null
    private val db = FirebaseFirestore.getInstance()
    private val model: UserViewModel by viewModels {
        val repo = UserRepository.getInstance(db, applicationContext)
        UserViewModelFactory(repo)
    }
    private lateinit var navView: BottomNavigationView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        navView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.nav_host_fragment)
        navView.selectedItemId = R.id.navigation_home

        if (!NetworkUtil().isNetworkAvailable(this)) {
            val snackbar = Snackbar.make(
                container, getString(R.string.noInternet),
                Snackbar.LENGTH_INDEFINITE
            ).setAction(getString(R.string.aceptar), View.OnClickListener { })
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()
        }

        userEmail = intent.getStringExtra(SessionManager.KEY_EMAIL)

        navController.addOnDestinationChangedListener { _, dest, _ ->
            when (dest.id) {
                R.id.navigation_profile -> {
                    dest.addArgument(
                        "user",
                        NavArgument.Builder()
                            .setDefaultValue(user ?: model.getUser().value)
                            .setType(NavType.SerializableType(User::class.java))
                            .build()
                    )
                }
            }
        }

        navView.setOnNavigationItemSelectedListener { mi ->
            when (mi.itemId) {
                R.id.navigation_profile -> {
                    val args = Bundle()
                    args.putSerializable("user", user)
                    navController.navigate(R.id.navigation_profile, args)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_home -> {
                    navController.navigate(R.id.navigation_home)
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.navigation_market -> {
                    navController.navigate(R.id.navigation_market)
                    return@setOnNavigationItemSelectedListener true
                }
                else -> {
                    return@setOnNavigationItemSelectedListener true
                }
            }
        }
        // navView.selectedItemId = R.id.navigation_home
        // navView.setupWithNavController(navController)

        model.getUser().observe(this, Observer<User> {
            // TODO Loader

            user = it
        })
        user ?: run {
            model.loadUser(userEmail!!)
        }
    }

    override fun onBackPressed() {
        if (state == 1) {
            navView.visibility = View.VISIBLE
        } else if (state == 2) {
            state = 1
        }
        this.supportFragmentManager.popBackStack()
    }

    override fun openNewMember() {
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            state = 1
            val args = Bundle()
            args.putBoolean(EditProfileFragment.ADD_MEMBER, true)
            args.putString(EditProfileFragment.EMAIL, userEmail)
            findNavController(R.id.nav_host_fragment).navigate(R.id.editProfileFragment, args)
            navView.visibility = View.GONE
        } else {
            val snackbar = Snackbar.make(
                findViewById(R.id.nav_view),
                getString(R.string.noIfOffline),
                Snackbar.LENGTH_INDEFINITE
            ).setAction(getString(R.string.aceptar), View.OnClickListener { })
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()
        }
    }

    override fun editProfile() {
        state = 1
        val args = Bundle()
        args.putSerializable(EditProfileFragment.MEMBER, user as Serializable)
        args.putString(EditProfileFragment.EMAIL, userEmail)
        args.putInt(EditProfileFragment.MEMBER_POS, 0)
        findNavController(R.id.nav_host_fragment).navigate(R.id.editProfileFragment, args)
        navView.visibility = View.GONE
    }

    override fun onMemberClick(member: Member, position: String) {
        state = 1
        val args = Bundle()
        args.putSerializable(EditProfileFragment.MEMBER, member as Serializable)
        args.putString(EditProfileFragment.EMAIL, userEmail)
        args.putString(EditProfileFragment.MEMBER_POS, position)
        findNavController(R.id.nav_host_fragment).navigate(R.id.editProfileFragment, args)
        navView.visibility = View.GONE
    }

    override fun profileUpdated(original: Member, member: Member, email: String, position: String) {
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            val upUser = User(name = member.name, objective = member.objective, email = email)
            if (original.hasChanged(upUser)) {
                model.updateUser(upUser, userEmail!!, position) {
                    original.apply {
                        name = upUser.name
                        objective = upUser.objective
                    }
                    goToProfile()
                    Toast.makeText(applicationContext, getString(R.string.saved), Toast.LENGTH_LONG)
                        .show()
                }
            } else {
                goToProfile()
                Toast.makeText(applicationContext, getString(R.string.noChanges), Toast.LENGTH_LONG)
                    .show()
            }
        } else {
            Snackbar.make(
                findViewById(R.id.nav_view),
                getString(R.string.noInternet),
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(getString(R.string.aceptar), View.OnClickListener { })
                .setActionTextColor(Color.WHITE)
                .show()

            goToProfile()
        }
    }

    override fun newMemberAdded(member: Member, email: String) {
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            model.addMember(member, email) {
                user!!.members[it.id] = it
                goToProfile()
            }
        } else {
            Snackbar.make(
                findViewById(R.id.nav_view),
                getString(R.string.noInternet),
                Snackbar.LENGTH_INDEFINITE
            )
                .setAction(getString(R.string.aceptar), View.OnClickListener { })
                .setActionTextColor(Color.WHITE)
                .show()
            goToProfile()
        }
    }

    private fun goToProfile() {
        state = 0
        val args = Bundle()
        args.putSerializable("user", user)
        supportFragmentManager.popBackStack()
        supportFragmentManager.popBackStack()
        findNavController(R.id.nav_host_fragment).navigate(R.id.navigation_profile, args)
        navView.visibility = View.VISIBLE
    }

    override fun addInclude(actualMember: Member, position: String, isAdding: Boolean) {
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            model.getFood {
                openFood(actualMember, position, isAdding, it.toTypedArray(), true)
            }
        } else {
            openFood(
                actualMember,
                position,
                isAdding,
                actualMember.preferences.likes.toTypedArray(),
                true
            )
        }
    }

    override fun addExclude(actualMember: Member, position: String, isAdding: Boolean) {
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            model.getFood {
                openFood(actualMember, position, isAdding, it.toTypedArray(), false)
            }
        } else {
            openFood(
                actualMember,
                position,
                isAdding,
                actualMember.preferences.dislikes.toTypedArray(),
                false
            )
        }
    }

    override fun saveIncludes(
        items: List<Food>,
        actualMember: Member,
        position: String,
        isAdding: Boolean
    ) {
        state = 1
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            model.savePreferences(userEmail!!, position, true, items.map { it.name }) {
                actualMember.preferences.likes = items.map { it.name }

                val args = Bundle()
                args.putSerializable(EditProfileFragment.MEMBER, actualMember as Serializable)
                args.putString(EditProfileFragment.EMAIL, userEmail)
                args.putString(EditProfileFragment.MEMBER_POS, position)
                args.putBoolean(EditProfileFragment.ADD_MEMBER, isAdding)
                supportFragmentManager.popBackStack()
                supportFragmentManager.popBackStack()
                findNavController(R.id.nav_host_fragment).navigate(R.id.editProfileFragment, args)
            }
        } else {
            val snackbar = Snackbar.make(
                findViewById(R.id.nav_view),
                getString(R.string.noInternet),
                Snackbar.LENGTH_INDEFINITE
            ).setAction(getString(R.string.aceptar), View.OnClickListener { })
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()

            val args = Bundle()
            args.putSerializable(EditProfileFragment.MEMBER, actualMember as Serializable)
            args.putString(EditProfileFragment.EMAIL, userEmail)
            args.putString(EditProfileFragment.MEMBER_POS, position)
            args.putBoolean(EditProfileFragment.ADD_MEMBER, isAdding)
            supportFragmentManager.popBackStack()
            supportFragmentManager.popBackStack()
            findNavController(R.id.nav_host_fragment).navigate(R.id.editProfileFragment, args)
        }
    }

    override fun saveExcludes(
        items: List<Food>,
        actualMember: Member,
        position: String,
        isAdding: Boolean
    ) {
        state = 1
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            model.savePreferences(userEmail!!, position, true, items.map { it.name }) {
                actualMember.preferences.dislikes = items.map { it.name }

                val args = Bundle()
                args.putSerializable(EditProfileFragment.MEMBER, actualMember as Serializable)
                args.putString(EditProfileFragment.EMAIL, userEmail)
                args.putString(EditProfileFragment.MEMBER_POS, position)
                args.putBoolean(EditProfileFragment.ADD_MEMBER, isAdding)
                supportFragmentManager.popBackStack()
                supportFragmentManager.popBackStack()
                findNavController(R.id.nav_host_fragment).navigate(R.id.editProfileFragment, args)
            }
        } else {
            val snackbar = Snackbar.make(
                findViewById(R.id.nav_view),
                getString(R.string.noInternet),
                Snackbar.LENGTH_INDEFINITE
            ).setAction(getString(R.string.aceptar), View.OnClickListener { })
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()

            val args = Bundle()
            args.putSerializable(EditProfileFragment.MEMBER, actualMember as Serializable)
            args.putString(EditProfileFragment.EMAIL, userEmail)
            args.putString(EditProfileFragment.MEMBER_POS, position)
            args.putBoolean(EditProfileFragment.ADD_MEMBER, isAdding)
            supportFragmentManager.popBackStack()
            supportFragmentManager.popBackStack()
            findNavController(R.id.nav_host_fragment).navigate(R.id.editProfileFragment, args)
        }
    }

    override fun budgetUpdated(email: String, budget: Int, onError: () -> Unit) {
        if (NetworkUtil().isNetworkAvailable(applicationContext)) {
            model.saveBudget(email, budget, onError = onError) {
                Toast.makeText(
                    applicationContext,
                    getString(R.string.budgetUpdated),
                    Toast.LENGTH_LONG
                )
                    .show()
            }
        } else {
            onError()
            val snackbar = Snackbar.make(
                findViewById(R.id.nav_view),
                getString(R.string.noInternet),
                Snackbar.LENGTH_INDEFINITE
            ).setAction(getString(R.string.aceptar), View.OnClickListener { })
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()
        }
    }

    private fun openFood(
        actualMember: Member,
        position: String,
        isAdding: Boolean,
        original: Array<String>,
        likes: Boolean
    ) {
        state = 2
        val args = Bundle()
        args.putBoolean(FoodFragment.INCLUDE, likes)
        args.putStringArray(
            FoodFragment.ADDED,
            (if (likes) {
                actualMember.preferences.likes
            } else {
                actualMember.preferences.dislikes
            }).toTypedArray()
        )
        args.putSerializable(FoodFragment.PREV_STATE, actualMember as Serializable)
        args.putString(FoodFragment.MEMBER_POS, position)
        args.putBoolean(FoodFragment.IS_ADDING, isAdding)
        args.putStringArray(
            FoodFragment.LIST,
            original
                .filter {
                    !(if (likes) {
                        actualMember.preferences.dislikes
                    } else {
                        actualMember.preferences.likes
                    }).contains(it)
                }
                .toTypedArray()
        )
        findNavController(R.id.nav_host_fragment).navigate(R.id.foodFragment, args)
    }

    override fun refreshProfile(cbk: (User?) -> Unit) {
        if (NetworkUtil().isNetworkAvailable(this)) {
            model.retrieveUser(userEmail!!, cbk)
        } else {
            cbk(null)
        }
    }
}
