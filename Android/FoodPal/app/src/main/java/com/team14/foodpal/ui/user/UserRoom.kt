package com.team14.foodpal.ui.user

import android.content.Context
import android.os.AsyncTask
import androidx.room.*
import com.team14.foodpal.util.*
import java.util.concurrent.Executor

@Database(entities = [User::class], version = 1, exportSchema = false)
@TypeConverters(
    MemberConverter::class,
    ShoppingListConverter::class,
    PreferenceConverter::class,
    ObjectiveConverter::class
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance
                ?: synchronized(AppDatabase::class) {
                    instance ?: Room.databaseBuilder(
                        context,
                        AppDatabase::class.java,
                        "foodpalDB.db"
                    ).build().also { instance = it }
                }
        }

        fun destroy() {
            instance = null
        }
    }
}

@Dao
interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(user: User)

    @Query("SELECT * FROM user WHERE email = :email")
    fun load(email: String): User?

    @Update
    fun update(user: User)
}