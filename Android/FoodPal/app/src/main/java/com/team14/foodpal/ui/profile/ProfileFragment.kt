package com.team14.foodpal.ui.profile

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SeekBar
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.snackbar.Snackbar
import com.team14.foodpal.R
import com.team14.foodpal.ui.profile.member.MembersRecyclerViewAdapter
import com.team14.foodpal.util.Member
import com.team14.foodpal.util.User
import kotlinx.android.synthetic.main.content_profile.view.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import java.text.NumberFormat
import java.util.*

fun parseDate(date: Calendar, res: Resources): String {
    val month = res.getStringArray(R.array.months)[date.get(Calendar.MONTH)]
    val year = date.get(Calendar.YEAR)
    val day = date.get(Calendar.DAY_OF_MONTH)
    return "$month $day, $year"
}

fun formatCurrency(value: Int): String {
    val format: NumberFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
    return format.format(value)
}

class ProfileFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {

    private var memberListener: OnMemberClick? = null
    private var newMemberCallback: OnNewMember? = null
    private var editProfileCallback: OnEditProfile? = null
    private var budgetUpdateListener: OnBudgetUpdate? = null
    private var refreshLayout: SwipeRefreshLayout? = null
    private var refreshListener: OnRefreshProfile? = null
    private var user: User? = null
    private var budgetValue: Int = 0
    private var memoView: View? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        user = arguments?.get("user") as User
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_profile, container, false)

        view.seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            var temp: Int = 0
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                budgetValue = (i / 1000) * 1000
                view.presupuesto_value.text = formatCurrency(budgetValue)
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                temp = seekBar.progress
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {
                budgetUpdateListener?.budgetUpdated(user!!.email, budgetValue) {
                    seekBar.progress = temp
                }
            }
        })
        view.fab_edit_profile.setOnClickListener { editProfile() }
        view.addMember.setOnClickListener {
            addMember()
        }

        refreshLayout = view.refresh_profile
        refreshLayout?.setOnRefreshListener(this)

        setupProfileData(view)
        setupMembers(view.members_container)
        memoView = view
        return view
    }

    private fun addMember() {
        newMemberCallback?.openNewMember()
    }

    private fun editProfile() {
        editProfileCallback?.editProfile()
    }

    private fun setupProfileData(view: View) {
        user?.let {
            view.miembros_value.text = it.members.size.toString()

            view.lastmarket_date.text = parseDate(Calendar.getInstance(), resources)

            view.profile_name.text = it.name
            view.profile_type.text = it.objective.str

            view.presupuesto_value.text = formatCurrency(it.budget)
            view.seekBar.progress = it.budget
        }
    }

    private fun setupMembers(view: View) {
        if (view is RecyclerView) {
            with(view) {
                setHasFixedSize(true) // Remove if not updating
                layoutManager = GridLayoutManager(context, 2)
                adapter = MembersRecyclerViewAdapter(
                    user?.members?.map { it.value }?.sortedBy { it.name } ?: listOf(),
                    memberListener
                )
                isNestedScrollingEnabled = false
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnNewMember) {
            newMemberCallback = context
        }
        if (context is OnEditProfile) {
            editProfileCallback = context
        }
        if (context is OnMemberClick) {
            memberListener = context
        }
        if (context is OnBudgetUpdate) {
            budgetUpdateListener = context
        }
        if (context is OnRefreshProfile) {
            refreshListener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        newMemberCallback = null
        editProfileCallback = null
        memberListener = null
    }

    override fun onRefresh() {
        refreshListener?.let {
            it.refreshProfile { refreshedUser ->
                if (refreshedUser != null) {
                    this.user = refreshedUser
                    this.setupProfileData(memoView!!)
                    this.setupMembers(memoView!!.members_container)
                    refreshLayout?.isRefreshing = false
                } else {
                    val snackbar = Snackbar.make(
                        memoView!!,
                        getString(R.string.noInternet),
                        Snackbar.LENGTH_INDEFINITE
                    ).setAction(getString(R.string.aceptar), View.OnClickListener { })
                    snackbar.setActionTextColor(Color.WHITE)
                    snackbar.show()
                    refreshLayout?.isRefreshing = false
                }
            }
        }
    }

    interface OnEditProfile {
        fun editProfile()
    }

    interface OnNewMember {
        fun openNewMember()
    }

    interface OnMemberClick {
        fun onMemberClick(member: Member, position: String)
    }

    interface OnBudgetUpdate {
        fun budgetUpdated(email: String, budget: Int, onError: () -> Unit)
    }

    interface OnRefreshProfile {
        fun refreshProfile(cbk: (User?) -> Unit)
    }
}