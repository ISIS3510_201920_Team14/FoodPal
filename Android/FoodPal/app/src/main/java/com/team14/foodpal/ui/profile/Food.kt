package com.team14.foodpal.ui.profile

import java.io.Serializable

data class Food(val index: Int, val name: String, var added: Boolean = false): Serializable
