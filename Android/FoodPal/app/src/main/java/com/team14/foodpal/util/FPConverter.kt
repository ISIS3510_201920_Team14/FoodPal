package com.team14.foodpal.util

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

abstract class FPConverter<T> {
    protected val gson = Gson()

    abstract fun parse(data: String?): T?
    abstract fun stringify(objs: T?): String?
}

class MemberConverter : FPConverter<HashMap<String, RealMember>>() {
    @TypeConverter
    override fun parse(data: String?): HashMap<String, RealMember>? {
        data?.let {
            val listType = object : TypeToken<HashMap<String, RealMember>>() {}.type
            return gson.fromJson(data, listType)
        } ?: run {
            return HashMap()
        }
    }

    @TypeConverter
    override fun stringify(objs: HashMap<String, RealMember>?): String? {
        return gson.toJson(objs)
    }
}


class ShoppingListConverter : FPConverter<List<ShoppingList>>() {
    @TypeConverter
    override fun parse(data: String?): List<ShoppingList>? {
        data?.let {
            val listType = object : TypeToken<List<ShoppingList>>() {}.type
            return gson.fromJson(data, listType)
        } ?: run {
            return listOf()
        }
    }

    @TypeConverter
    override fun stringify(objs: List<ShoppingList>?): String? {
        return gson.toJson(objs)
    }
}

class PreferenceConverter : FPConverter<Preferences>() {

    @TypeConverter
    override fun parse(data: String?): Preferences? {
        data?.let {
            return gson.fromJson(data, Preferences::class.java)
        } ?: run {
            return Preferences()
        }
    }

    @TypeConverter
    override fun stringify(objs: Preferences?): String? {
        return gson.toJson(objs)
    }
}

class ObjectiveConverter : FPConverter<Objectives>() {

    @TypeConverter
    override fun parse(data: String?): Objectives? {
        data?.let {
            return Objectives.fromStr(data)
        }
        return Objectives.NONE
    }

    @TypeConverter
    override fun stringify(objs: Objectives?): String? {
        return objs?.toStrDB()
    }
}
