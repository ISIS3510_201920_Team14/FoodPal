package com.team14.foodpal.ui.recetas


import android.os.Bundle
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button

import android.widget.LinearLayout
import android.widget.TextView
import com.team14.foodpal.R


class IngredientsFragment(grupo: Map<String, String>) : Fragment() {

    var grupo = grupo
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Log.d("CREATE","ENTRA A CREATE")
        return inflater.inflate(R.layout.fragment_ingredients, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val linearLayout1 = getView()?.findViewById(R.id.linearIngredientes1) as LinearLayout
        val linearLayout2 = getView()?.findViewById(R.id.linearIngredientes2) as LinearLayout


        Log.d("VIEW",grupo.size.toString())

        for (x in 0 until grupo.size)
        {
            val tv_dynamic = TextView(this.context)
            val tv_dynamic2 = TextView(this.context)
            val ingredientes = grupo.get(x.toString()) as Map<String, String>
            val nombre = ingredientes.get("name")
            val cantidad = ingredientes.get("quantity")
            tv_dynamic.text=nombre
            tv_dynamic2.text=cantidad
            tv_dynamic.setBackgroundResource(R.drawable.ingredients_list_left)
            tv_dynamic2.setBackgroundResource(R.drawable.ingredients_list_right)
            tv_dynamic.textSize=20f
            tv_dynamic2.textSize=20f
            tv_dynamic.gravity= Gravity.CENTER
            tv_dynamic2.gravity= Gravity.CENTER

            val layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
            )


            layoutParams.setMargins(0, 20, 0, 10)

            tv_dynamic.layoutParams = layoutParams
            tv_dynamic2.layoutParams = layoutParams

            linearLayout1.addView(tv_dynamic)
            linearLayout2.addView(tv_dynamic2)
            Log.d("INSIDE FRAGMENT", nombre.toString())
        }
    }


}
