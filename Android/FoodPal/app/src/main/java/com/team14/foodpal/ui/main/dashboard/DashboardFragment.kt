package com.team14.foodpal.ui.main.dashboard

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.team14.foodpal.R
import com.team14.foodpal.ui.maps.MapsActivity
import com.team14.foodpal.ui.recetas.RecetasActivity
import com.team14.foodpal.ui.tips.ActivityTips
import com.team14.foodpal.util.NetworkUtil
import android.widget.*
import com.team14.foodpal.ui.listaCompras.ListaComprasActivity


class DashboardFragment : Fragment() {

    private lateinit var dashboardViewModel: DashboardViewModel
    private var v: View? = null


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dashboardViewModel =
            ViewModelProviders.of(this).get(DashboardViewModel::class.java)
        val root = inflater.inflate(com.team14.foodpal.R.layout.fragment_dashboard, container, false)
        val textView: TextView = root.findViewById(com.team14.foodpal.R.id.text_dashboard)
        val botonMirarRecetas: ImageButton = root.findViewById(com.team14.foodpal.R.id.imageView6)
        v = inflater.inflate(com.team14.foodpal.R.layout.fragment_dashboard, container, false);
        dashboardViewModel.text.observe(this, Observer {
            textView.text = it
        })
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val botonMirarRecetas: ImageButton = activity?.findViewById(com.team14.foodpal.R.id.imageView6)!!
        mirarRecetas(botonMirarRecetas)

        val botonTips: ImageButton = activity?.findViewById(com.team14.foodpal.R.id.imageView7)!!
        verTips(botonTips)

        val botonMaps = activity?.findViewById<ImageButton>(com.team14.foodpal.R.id.supermarkets_btn)!!
        viewMap(botonMaps)

        val botonLista = activity?.findViewById<ImageButton>(com.team14.foodpal.R.id.marketplan)!!
        verListaCompras(botonLista)

    }

    private fun verListaCompras(botonLista: ImageButton) {
        botonLista.setOnClickListener {

            if (NetworkUtil().isNetworkAvailable(this.context!!)) {
                botonLista.isEnabled = false
                val intent = Intent(this.context, ListaComprasActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this.context!!, "No hay conexión a internet!", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }

    fun verTips(botonTips: ImageButton) {
        botonTips.setOnClickListener {

            if (NetworkUtil().isNetworkAvailable(this.context!!)) {
                botonTips.isEnabled = false
                val intent = Intent(this.context, ActivityTips::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this.context!!, "No hay conexión a internet!", Toast.LENGTH_LONG)
                    .show()
            }
        }
    }


    fun mirarRecetas(botonMirarRecetas: ImageView) {
        botonMirarRecetas.setOnClickListener {
            botonMirarRecetas.isEnabled = false
            val intent = Intent(this.context, RecetasActivity::class.java)
            startActivity(intent)
        }
    }

    private fun viewMap(btnView: ImageButton) {
        btnView.setOnClickListener {
            btnView.isEnabled = false
            val intent = Intent(this.context, MapsActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onResume() {
        val botonTips: ImageButton = activity?.findViewById(com.team14.foodpal.R.id.imageView7)!!

        val botonMirarRecetas: ImageButton = activity?.findViewById(com.team14.foodpal.R.id.imageView6)!!

        val botonMaps = activity?.findViewById<ImageButton>(com.team14.foodpal.R.id.supermarkets_btn)!!

        val botonLista = activity?.findViewById<ImageButton>(com.team14.foodpal.R.id.marketplan)!!


        botonMaps.isEnabled = true
        botonTips.isEnabled = true
        botonMirarRecetas.isEnabled = true
        botonLista.isEnabled = true
        super.onResume()
    }
}