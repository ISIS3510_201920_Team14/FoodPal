package com.team14.foodpal.ui.auth

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.team14.foodpal.ui.MainActivity
import com.google.firebase.auth.FirebaseAuth
import androidx.appcompat.app.AlertDialog
import com.team14.foodpal.R
import com.team14.foodpal.util.NetworkUtil
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_choose.*


class ChooseActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    lateinit var session: SessionManager


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.team14.foodpal.R.layout.activity_choose)

        auth = FirebaseAuth.getInstance()


        val alertDialogBuilder = AlertDialog.Builder(this)
        val loginButton: Button = findViewById(R.id.button) as Button
        val signinbutton: Button = findViewById(R.id.button2) as Button


        auth.currentUser?.let {
            val intent = Intent(this, MainActivity::class.java)
            intent.putExtra(SessionManager.KEY_EMAIL, it.email)
            startActivity(intent)
            finish()
        } ?: run {
            signinbutton.setOnClickListener {
                if (NetworkUtil().isNetworkAvailable(this@ChooseActivity)) {
                    val intent = Intent(this, LoginActivity::class.java)
                    startActivity(intent)
                } else {
                    val snackbar = Snackbar.make(
                        rootlayout, "No hay conexión a internet!",
                        Snackbar.LENGTH_LONG
                    ).setAction("Aceptar", View.OnClickListener { })
                    snackbar.setActionTextColor(Color.WHITE)
                    snackbar.show()
                }


            }


            loginButton.setOnClickListener{
                if(NetworkUtil().isNetworkAvailable(this@ChooseActivity))
                {
                val intent = Intent(this,SignupActivity::class.java)
                startActivity(intent)
                }
                else
                {
                    val snackbar = Snackbar.make(rootlayout, "No hay conexión a internet!",
                            Snackbar.LENGTH_INDEFINITE).setAction("Aceptar", View.OnClickListener {  })
                    snackbar.setActionTextColor(Color.WHITE)
                    snackbar.show()
                }
            }
        }
    }
}
