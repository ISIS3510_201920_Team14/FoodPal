package com.team14.foodpal.ui.listaCompras

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.gson.GsonBuilder
import com.team14.foodpal.R
import com.team14.foodpal.util.NetworkUtil
import kotlinx.android.synthetic.main.activity_lista_compras.*
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONObject
import okhttp3.MediaType

import okhttp3.RequestBody

import java.util.concurrent.CountDownLatch









class ListaComprasActivity : AppCompatActivity() {

    var db = FirebaseFirestore.getInstance()
    private lateinit var auth: FirebaseAuth

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.team14.foodpal.R.layout.activity_lista_compras)
        getListaCompras().execute()
    }

    internal inner class getListaCompras: AsyncTask<Void,Void,String>()
    {
        lateinit var progressBar:ProgressBar
        override fun onPreExecute() {
            super.onPreExecute()

        }
        override fun doInBackground(vararg p0: Void?): String {
            if(NetworkUtil().isNetworkAvailable(this@ListaComprasActivity))
            {
                val done = CountDownLatch(1)

                auth = FirebaseAuth.getInstance()
                val userActual = auth.currentUser!!.email.toString()
                val docRef = db.collection("users").document(userActual)

                var budget = ""
                var liked = ""
                var disliked = ""
                var objective = ""

                docRef.get()
                    .addOnSuccessListener { document ->
                        if (document != null) {
                            budget = document.get("budget").toString()
                            objective = document.get("objective") as String

                            done.countDown()
                        } else {
                            Log.d("ERROR", "No such document")
                        }
                    }
                    .addOnFailureListener { exception ->
                        Log.d("ERROR", "get failed with ", exception)
                    }

                try {
                    done.await() //it will wait till the response is received from firebase.
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }


                val JSON = MediaType.parse("application/json; charset=utf-8")
                val params = HashMap<String, Any>()
                params["liked"] = liked
                params["disliked"] = disliked
                params["objective"] = objective
                params["budget"] = budget.toInt()
                val parameter = JSONObject(params as Map<Any, Any>)

                val body = RequestBody.create(JSON, parameter.toString())

                val client = OkHttpClient()
                val url = "http://foodpal-back.herokuapp.com/api/generate/"
                val request = Request.Builder().url(url).post(body).build()
                val response = client.newCall(request).execute()


                var re = response.body()?.string().toString()


                return re
            }
            return ""
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            var jSONResponse="{\"productos\":"+result+"}";

            val gson = GsonBuilder().setLenient().create()
            val objetos =  gson.fromJson(jSONResponse, ListaCompras::class.java)

            if (objetos.productos.size != 0)
            {
                val linearLayout1 = findViewById<LinearLayout>(R.id.linearLayoutListaCompras)
                var precioTotal = 0


                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                )

                for(x in 0 until objetos.productos.size)
                {
                    val tv_dynamic = TextView(applicationContext)
                    val tv_dynamic2 = TextView(applicationContext)

                    val nombreProducto = objetos.productos[x].name
                    val precio = objetos.productos[x].cost

                    precioTotal += precio
                    tv_dynamic.text = nombreProducto
                    tv_dynamic2.text = precio.toString()
                    tv_dynamic.setBackgroundResource(R.drawable.ingredients_list_left)
                    tv_dynamic2.setBackgroundResource(R.drawable.ingredients_list_right)
                    tv_dynamic.textSize=20f
                    tv_dynamic2.textSize=20f
                    tv_dynamic.gravity= Gravity.CENTER
                    tv_dynamic2.gravity= Gravity.CENTER



                    layoutParams.setMargins(0, 20, 0, 10)

                    tv_dynamic.layoutParams = layoutParams
                    tv_dynamic2.layoutParams = layoutParams

                    linearLayout1.addView(tv_dynamic)
                    linearLayout1.addView(tv_dynamic2)

                }
                val tv_dynamic3 = TextView(applicationContext)
                layoutParams.setMargins(0, 20, 0, 10)
                tv_dynamic3.textSize=20f
                tv_dynamic3.layoutParams = layoutParams
                tv_dynamic3.text = "Costo total del mercado: " + precioTotal
                linearLayout1.addView(tv_dynamic3)

//            .text = objetos.productos[0].name.toString()
            }

            else
            {
                val linearLayout1 = findViewById<LinearLayout>(R.id.linearLayoutListaCompras)


                val layoutParams = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT
                )

                val tv_dynamic3 = TextView(applicationContext)
                layoutParams.setMargins(0, 20, 0, 10)
                tv_dynamic3.textSize=20f
                tv_dynamic3.layoutParams = layoutParams
                tv_dynamic3.text = "Tu presupuesto es muy bajo para cualquier producto :("
                linearLayout1.addView(tv_dynamic3)
            }


        }
    }
}

class ListaCompras(val productos: List<Productos>)

class Productos (val id: Int, val codebar: Any, val name: String, val cost : Int)