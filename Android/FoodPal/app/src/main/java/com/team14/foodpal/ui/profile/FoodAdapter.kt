package com.team14.foodpal.ui.profile

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.team14.foodpal.R
import com.team14.foodpal.ui.profile.FoodFragment.OnFoodSave
import kotlinx.android.synthetic.main.fragment_food.view.*

/**
 * [RecyclerView.Adapter] that can display a [Food] and makes a call to the
 * specified [OnFoodSave].
 */
class FoodAdapter(
    private val mValues: List<Food>,
    private val checkBoxListener: OnCheckboxChange
) : RecyclerView.Adapter<FoodAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_food, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.box.isChecked = item.added
        holder.box.setOnCheckedChangeListener { _, isChecked ->
            checkBoxListener.chechboxChange(item.index, isChecked)
        }
        holder.box.text = item.name
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(private val mView: View) : RecyclerView.ViewHolder(mView) {
        val box: CheckBox = mView.checkBox

        override fun toString(): String {
            return super.toString() + " '" + box.text + "'"
        }
    }
}
