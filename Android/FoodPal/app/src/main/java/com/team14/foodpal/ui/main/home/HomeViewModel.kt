package com.team14.foodpal.ui.main.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class HomeViewModel : ViewModel() {

    private val _text = MutableLiveData<String>().apply {
        value = "¿Deseas escanear algun producto?"
    }
    val text: LiveData<String> = _text
}