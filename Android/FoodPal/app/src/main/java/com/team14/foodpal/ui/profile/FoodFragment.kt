package com.team14.foodpal.ui.profile

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.team14.foodpal.R
import com.team14.foodpal.util.Member
import kotlinx.android.synthetic.main.content_food_list.view.*
import kotlinx.android.synthetic.main.fragment_food_list.view.*

interface OnCheckboxChange {
    fun chechboxChange(index: Int, state: Boolean)
}

/**
 * A fragment representing a list of Items.
 * Activities containing this fragment MUST implement the
 * [FoodFragment.OnFoodSave] interface.
 */
class FoodFragment : Fragment(), OnCheckboxChange {

    private var member: Member? = null
    private var include = false
    private var firstAdded: Array<String>? = arrayOf()
    private var memberPos: String = ""
    private var isAdding: Boolean = false

    private var listener: OnFoodSave? = null

    private var originalList: List<Food> = listOf()
    private var shownList: MutableList<Food> = mutableListOf()
    private lateinit var fView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.apply {
            getBoolean(INCLUDE).let { include = it }
            getStringArray(ADDED)?.let { firstAdded = it }
            getSerializable(PREV_STATE)?.let { member = it as Member }
            getString(MEMBER_POS, "").let { memberPos = it }
            getBoolean(IS_ADDING, false).let { isAdding = it }
            getStringArray(LIST)?.let {
                originalList = it.mapIndexed { i, s -> Food(i, s) }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        fView = inflater.inflate(R.layout.fragment_food_list, container, false)

        // originalList = dummyFood.map { it.copy() }
        originalList.forEach {
            firstAdded?.apply {
                if (contains(it.name)) {
                    it.added = true
                }
            }
        }
        originalList.forEach { shownList.add(it.copy()) }

        // Set the adapter
        val self = this
        if (fView.list is RecyclerView) {
            with(fView.list) {
                layoutManager = LinearLayoutManager(context)
                adapter = FoodAdapter(shownList, self)
            }
        }
        fView.fab_save_food.setOnClickListener { onSave() }
        fView.searchFood.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                p0?.let {
                    if (it.isEmpty()) {
                        shownList.clear()
                        originalList.forEach { i -> shownList.add(i.copy()) }
                        fView.list.adapter = FoodAdapter(shownList, self)
                        fView.list.adapter?.notifyDataSetChanged()
                    }

                    if (shownList.isEmpty()) {
                        fView.foodMsg.visibility = View.VISIBLE
                    } else {
                        fView.foodMsg.visibility = View.GONE
                    }

                    fView.list.adapter?.notifyDataSetChanged()
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                p0?.let {
                    shownList.clear()
                    originalList
                        .filter { it.name.contains(p0, ignoreCase = true) }
                        .forEach { shownList.add(it.copy()) }
                    fView.list.adapter = FoodAdapter(shownList, self)

                    if (shownList.isEmpty()) {
                        fView.foodMsg.visibility = View.VISIBLE
                    } else {
                        fView.foodMsg.visibility = View.GONE
                    }

                    fView.list.adapter?.notifyDataSetChanged()
                }
            }
        })
        if (shownList.isEmpty()) {
            fView.foodMsg.visibility = View.VISIBLE
        }
        return fView
    }

    private fun onSave() {
        fView.fab_save_food.isEnabled = false
        listener?.apply {
            val added = originalList.filter { it.added }
            if (include) {
                saveIncludes(added, member!!, memberPos, isAdding)
            } else {
                saveExcludes(added, member!!, memberPos, isAdding)
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFoodSave) {
            listener = context
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun chechboxChange(index: Int, state: Boolean) {
        originalList.filter { it.index == index }[0].added = state
    }

    interface OnFoodSave {
        fun saveIncludes(
            items: List<Food>,
            actualMember: Member,
            position: String,
            isAdding: Boolean
        )

        fun saveExcludes(
            items: List<Food>,
            actualMember: Member,
            position: String,
            isAdding: Boolean
        )
    }

    companion object {
        const val LIST = "list"
        const val INCLUDE = "include"
        const val ADDED = "added"
        const val PREV_STATE = "prev_state"
        const val MEMBER_POS = "member_pos"
        const val IS_ADDING = "is_adding"
    }
}
