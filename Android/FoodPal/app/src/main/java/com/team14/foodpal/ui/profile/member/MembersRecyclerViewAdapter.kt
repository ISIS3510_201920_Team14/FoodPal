package com.team14.foodpal.ui.profile.member

import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.team14.foodpal.R
import com.team14.foodpal.ui.profile.ProfileFragment
import com.team14.foodpal.util.Member
import com.team14.foodpal.util.RealMember


import kotlinx.android.synthetic.main.fragment_member.view.*
import org.w3c.dom.Text

/**
 * [RecyclerView.Adapter] that can display a [Member] and makes a call to the
 * specified [ProfileFragment.OnMemberClick].
 */
class MembersRecyclerViewAdapter(
    private val mValues: List<RealMember>,
    private val mListener: ProfileFragment.OnMemberClick?
) : RecyclerView.Adapter<MembersRecyclerViewAdapter.ViewHolder>() {

    private val mOnClickListener: (String) -> View.OnClickListener

    init {
        mOnClickListener = { position ->
            View.OnClickListener { v ->
                val item = v.tag as RealMember
                mListener?.onMemberClick(item, position)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.fragment_member, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mValues[position]
        holder.txtName.text = item.name
        holder.txtObjective.text = item.objective.str
        holder.txtLikes.text = item.preferences.likes.size.toString()
        holder.txtDislikes.text = item.preferences.dislikes.size.toString()

        with(holder.mView) {
            tag = item
            setOnClickListener(mOnClickListener(item.id))
        }
    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val txtName: TextView = mView.member_name
        val txtObjective: TextView = mView.member_plan
        val txtLikes: TextView = mView.member_likes
        val txtDislikes: TextView = mView.member_dislikes

        override fun toString(): String {
            return super.toString() + " '" + txtName.text + "'"
        }
    }
}
