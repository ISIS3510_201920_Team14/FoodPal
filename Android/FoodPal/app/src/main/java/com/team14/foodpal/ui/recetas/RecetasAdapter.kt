package com.team14.foodpal.ui.recetas

import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.team14.foodpal.R
import com.team14.foodpal.ui.MainActivity
import com.team14.foodpal.util.NetworkUtil
import kotlinx.android.synthetic.main.activity_recetas.view.*
import kotlinx.android.synthetic.main.lista_recetas.view.*

class RecetasAdapter(val items: ArrayList<String>, val context: Context) : RecyclerView.Adapter<RecetasAdapter.ViewHolder>() {

    var name = String()

    // Gets the number of recipes in the list
    override fun getItemCount(): Int {
        if(items!=null)
        {
            return items.size

        }
        return 0
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent?.context).inflate(R.layout.lista_recetas, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val receta:String = items[position]
        holder?.textViewName?.text  = receta
        holder?.receta = receta

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val textViewName = itemView.findViewById<View>(R.id.tv_lista_recetas) as TextView
        var receta = String()

        init{

                itemView.setOnClickListener{

                        val intent = Intent(itemView.context, RecetaActivity::class.java)
                        intent.putExtra("NAME_RECETA", receta)
                        itemView.context.startActivity(intent)

            }





        }
        // Holds the TextView that will add each recipe to



    }
}
