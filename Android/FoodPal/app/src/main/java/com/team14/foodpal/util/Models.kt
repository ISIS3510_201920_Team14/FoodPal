package com.team14.foodpal.util

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import java.io.Serializable
import java.util.*
import kotlin.collections.HashMap

enum class Objectives(val str: String) {
    NONE("-"),
    VEGETARIANO("Vegetariano"),
    BAJAR_PESO("Bajar Peso"),
    MANTENER_PESO("Mantener Peso"),
    AUMENTAR_PESO("Aumentar Peso");

    fun toStrDB(): String {
        return this.str.replace(' ', '_').toUpperCase(Locale.getDefault())
    }

    companion object {
        fun fromStr(s: String): Objectives {
            val data = s.replace(' ', '_').toUpperCase(Locale.getDefault())
            return valueOf(if (data == "-" || data == "TIPO_DE_ALIMENTACIÓN") "NONE" else data)
        }
    }
}

data class Ingredient(
    val name: String,
    val quantity: String
) : Serializable

data class Recipe(
    val id: String,
    val name: String?,
    val steps: List<String>?,
    val ingredients: List<Ingredient>?
) : Serializable

data class Preferences(
    var likes: List<String> = listOf(),
    var dislikes: List<String> = listOf()
) : Serializable

open class Member(
    var name: String = "",
    @TypeConverters(ObjectiveConverter::class)
    var objective: Objectives = Objectives.NONE,
    @TypeConverters(PreferenceConverter::class)
    var preferences: Preferences = Preferences()
) : Serializable {
    fun hasChanged(nU: User): Boolean {
        return nU.name != name || nU.objective != objective
    }
}

class RealMember(
    name: String = "",
    @TypeConverters(ObjectiveConverter::class)
    objective: Objectives = Objectives.NONE,
    @TypeConverters(PreferenceConverter::class)
    preferences: Preferences = Preferences(),
    val id: String = ""
) : Member(name, objective, preferences)

data class ShoppingList(
    var name: String,
    var items: List<Ingredient>,
    var members: List<String> // Member IDs
) : Serializable

@Entity
class User(
    name: String = "",
    @TypeConverters(ObjectiveConverter::class)
    objective: Objectives = Objectives.NONE,
    @TypeConverters(PreferenceConverter::class)
    preferences: Preferences = Preferences(),
    @PrimaryKey val email: String = "",
    var strata: Int = 0,
    @TypeConverters(MemberConverter::class)
    var members: HashMap<String, RealMember> = HashMap(),
    @TypeConverters(ShoppingListConverter::class)
    var shoppinglist: List<ShoppingList> = listOf(),
    var budget: Int = 0
) : Member(name, objective, preferences), Serializable
