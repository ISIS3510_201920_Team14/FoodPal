package com.team14.foodpal.ui.maps

import android.view.View
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import kotlinx.android.synthetic.main.window_map.view.*
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

class PlaceLatLng(val lat: Double, val lng: Double)

class PlaceGeometry(val location: PlaceLatLng)

class PlaceItem(
    val id: String,
    val name: String,
    val types: List<String>,
    private val geometry: PlaceGeometry,
    val vicinity: String
) {
    fun getLatLng(): LatLng {
        return LatLng(this.geometry.location.lat, this.geometry.location.lng)
    }
}

class PlaceResponse(
    val html_attributions: List<String>,
    val next_page_token: String,
    val status: String,
    val results: List<PlaceItem>,
    val error_message: String?
) {
    override fun toString(): String {
        if (this.status != "OK") {
            return "status: ${this.status}, " +
                    "error_message: ${this.error_message}"
        }
        return "status: ${this.status}, " +
                "results: ${this.results}, " +
                "html_attributions: ${this.html_attributions}, " +
                "next_page_token ${this.next_page_token}"
    }

    fun isOk(): Boolean {
        return this.status == "OK"
    }
}

interface PlacesApi {
    @GET("/maps/api/place/nearbysearch/json")
    fun getPlacesAsync(
        @Query("location") location: String,
        @Query("radius") radius: Int,
        @Query("type") type: String,
        @Query("key") key: String
    ): Call<PlaceResponse>
}

class MarkerWindow(
    private val activity: MapsActivity,
    private val layout: Int
) : GoogleMap.InfoWindowAdapter {
    override fun getInfoContents(marker: Marker?): View? {
        marker?.let {
            val view: View = this.activity.layoutInflater.inflate(this.layout, null)
            val vecinity = it.snippet.split("@@")[0]

            view.place_name.text = it.title
            view.place_address.text = vecinity

            return view
        }
        return null
    }

    override fun getInfoWindow(marker: Marker?): View? {
        return null
    }

}