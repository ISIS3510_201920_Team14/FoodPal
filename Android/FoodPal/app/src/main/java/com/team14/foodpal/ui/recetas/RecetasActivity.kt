package com.team14.foodpal.ui.recetas

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.team14.foodpal.R
import com.team14.foodpal.util.NetworkUtil
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_recetas.*
import kotlinx.android.synthetic.main.content_edit_profile.*

import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.widget.*


class RecetasActivity: AppCompatActivity( ) {


    private lateinit var auth: FirebaseAuth

    private lateinit var database: DatabaseReference// ...
    var db = FirebaseFirestore.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView( R.layout.activity_recetas)




        val listaRecetas = ArrayList<String>()

        var recetasFavoritas = ArrayList<String>()

        auth = FirebaseAuth.getInstance()
        val userActual = auth.currentUser!!.email.toString()

        val dodo = db.collection("users").document(userActual)


        db.collection("recipes").get()
            .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                if (task.isSuccessful) {

                    for (document in task.result!!) {
                        listaRecetas.add(document.id.toString())
                        Log.d("DOC", document.id.toString())
                    }


                    dodo.get()
                        .addOnSuccessListener { document ->
                            if (document != null) {
                                recetasFavoritas = document.get("recipes") as ArrayList<String>

                                Log.d("RECETAS FAVORITAS ",recetasFavoritas.size.toString())



                                val recyclerFavoritos = findViewById<RecyclerView>(R.id.listafavoritos)

                                recyclerFavoritos.layoutManager=LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
                                val adapterFavoritos = RecetasAdapter(recetasFavoritas,this)

                                recyclerFavoritos.adapter = adapterFavoritos


                            } else {
                                Log.d("NO HAY USER", "No such document")
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d("ERROR ", "get failed with ", exception)
                        }


                    val recyclerView = findViewById<RecyclerView>(R.id.listarecetas)

                    recyclerView.layoutManager=LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
                    val adapter = RecetasAdapter(listaRecetas,this)

                    recyclerView.adapter = adapter





                } else {
                    Log.d("ERROR", "Error getting documents: ", task.exception)
                }
            })




    }


    override fun onResume() {
        super.onResume()




        val listaRecetas = ArrayList<String>()

        var recetasFavoritas = ArrayList<String>()

        auth = FirebaseAuth.getInstance()
        val userActual = auth.currentUser!!.email.toString()

        val dodo = db.collection("users").document(userActual)


        db.collection("recipes").get()
            .addOnCompleteListener(OnCompleteListener<QuerySnapshot> { task ->
                if (task.isSuccessful) {

                    for (document in task.result!!) {
                        listaRecetas.add(document.id.toString())
                        Log.d("DOC", document.id.toString())
                    }


                    dodo.get()
                        .addOnSuccessListener { document ->
                            if (document != null) {
                                recetasFavoritas = document.get("recipes") as ArrayList<String>

                                Log.d("RECETAS FAVORITAS ",recetasFavoritas.size.toString())



                                val recyclerFavoritos = findViewById<RecyclerView>(R.id.listafavoritos)

                                recyclerFavoritos.layoutManager=LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
                                val adapterFavoritos = RecetasAdapter(recetasFavoritas,this)

                                recyclerFavoritos.adapter = adapterFavoritos


                            } else {
                                Log.d("NO HAY USER", "No such document")
                            }
                        }
                        .addOnFailureListener { exception ->
                            Log.d("ERROR ", "get failed with ", exception)
                        }


                    val recyclerView = findViewById<RecyclerView>(R.id.listarecetas)

                    recyclerView.layoutManager=LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false)
                    val adapter = RecetasAdapter(listaRecetas,this)

                    recyclerView.adapter = adapter





                } else {
                    Log.d("ERROR", "Error getting documents: ", task.exception)
                }
            })


    }




}