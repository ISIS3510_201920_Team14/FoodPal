package com.team14.foodpal.ui.main.home

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.firebase.firestore.FirebaseFirestore
import com.google.zxing.integration.android.IntentIntegrator
import com.team14.foodpal.R
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.*
import android.R.attr.data
import android.net.Uri
import com.google.zxing.integration.android.IntentResult
import com.team14.foodpal.util.NetworkUtil
import io.github.rybalkinsd.kohttp.ext.httpGet
import okhttp3.Response
import java.net.URL
import java.net.URLEncoder
import android.os.AsyncTask.execute
import android.util.Log
import android.view.Gravity
import android.widget.*
import io.github.rybalkinsd.kohttp.dsl.async.httpGetAsync
import okhttp3.Request
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.runOnUiThread
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.uiThread
import org.json.JSONArray
import org.json.JSONObject
import java.util.concurrent.Executors


class HomeFragment : Fragment() {

    private lateinit var homeViewModel: HomeViewModel
    var db = FirebaseFirestore.getInstance()
    var httpGetResult = String()
    var jota = JSONObject()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        homeViewModel =
            ViewModelProviders.of(this).get(HomeViewModel::class.java)
        val root = inflater.inflate(com.team14.foodpal.R.layout.fragment_home, container, false)
        val textView: TextView = root.findViewById(com.team14.foodpal.R.id.text_home)
        homeViewModel.text.observe(this, Observer {
            textView.text = it
        })

        val button = root.findViewById(com.team14.foodpal.R.id.buttonScan) as Button
        val textView222 = root.findViewById(com.team14.foodpal.R.id.resultText) as TextView
        button.setOnClickListener{

            if(NetworkUtil().isNetworkAvailable(this.context!!)) {


                val scanner = IntentIntegrator.forSupportFragment(this)

                scanner.initiateScan()
          }
            else{
                Toast.makeText(activity!!.applicationContext,"¡No hay conexión a internet!",
                    Toast.LENGTH_LONG).show()
            }

        }


        return root
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if(NetworkUtil().isNetworkAvailable(this.context!!))
        {
            if(resultCode==Activity.RESULT_OK)
            {
                val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
                if (result != null) {

                    if (result.contents == null) {
                        Toast.makeText(this.context, "Cancelled", Toast.LENGTH_LONG).show()
                    } else {
                        httpGetResult= result.contents

                        Log.d("HEY",httpGetResult)

                        var reqParam = result.contents

                        val result ="http://foodpal-back.herokuapp.com/api/products/codebar/"+reqParam


                        Executors.newSingleThreadExecutor().execute {
                            var json = URL(result).readText()


                            json = json.replace("[", "")
                            json = json.replace("]", "")

                            if(json.equals(""))
                            {
                                resultText.post { resultText.text = "¡No se ha encontrado el producto!" }
                                value.post{value.text = ""}
                            }
                            else
                            {
                                jota = JSONObject(json)
                                resultText.post { resultText.text = jota.getString("name") }
                                value.post{value.text = "$"+jota.getString("cost")}
                            }


                        }




                    }
                } else {
                    super.onActivityResult(requestCode, resultCode, data)
                }
            }
        }
        else
        {
            toast("No hay conexión a internet")

        }

    }


}