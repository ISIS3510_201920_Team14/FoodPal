package com.team14.foodpal.ui.auth

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import android.content.Intent
import android.graphics.Color
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.team14.foodpal.databinding.ActivityLoginBinding
import com.team14.foodpal.util.toast
import com.google.firebase.database.DatabaseReference
import kotlinx.android.synthetic.main.activity_signup.*

import com.google.firebase.firestore.FirebaseFirestore
import com.team14.foodpal.util.NetworkUtil
import kotlinx.android.synthetic.main.activity_login.*


class SignupActivity : AppCompatActivity() {


    private lateinit var auth: FirebaseAuth
    private lateinit var database: DatabaseReference// ...
    var db = FirebaseFirestore.getInstance()


    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        val binding: ActivityLoginBinding =
            DataBindingUtil.setContentView(this, com.team14.foodpal.R.layout.activity_login)
        val viewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)



        binding.viewmodel = viewModel

        setContentView(com.team14.foodpal.R.layout.activity_signup)



        auth = FirebaseAuth.getInstance()


        add_member.setOnClickListener {
            add_member.isEnabled = false
            signUpUser()

        }
    }

    private fun signUpUser() {
        val alertDialogBuilder = AlertDialog.Builder(this)

        val userNamee = editText2.text.toString()
        if (userNamee.isNullOrEmpty()) {
            add_member.isEnabled = true
            editText2.setError("El nombre no puede estar vacío.")
            editText2.requestFocus()
            return
        }
        val email = editText4.text.toString()
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            add_member.isEnabled = true

            editText4.setError("El Correo electrónico es invalido.")
            editText4.requestFocus()
            return
        }
        if (email.isNullOrEmpty()) {
            add_member.isEnabled = true

            editText4.setError("El correo electrónico no puede estar vacío.")
            editText4.requestFocus()
            return
        }
        if (editText6.text.toString().isNullOrEmpty()) {
            add_member.isEnabled = true

            editText6.setError("La contraseña no puede estar vacía.")
            editText6.requestFocus()
            return
        }


        if (NetworkUtil().isNetworkAvailable(this@SignupActivity)) {
            auth.createUserWithEmailAndPassword(
                editText4.text.toString(),
                editText6.text.toString()
            ).addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {

                        writeNewUser(userNamee, email)
                        alertDialogBuilder.setTitle("Cuenta Creada")
                        alertDialogBuilder.setMessage("¡Cuenta Creada de forma satisfactoria!")


                        // Display a neutral button on alert dialog
                        alertDialogBuilder.setNeutralButton("Entendido.") { _, _ ->
                            Toast.makeText(
                                applicationContext,
                                "You cancelled the dialog.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        // Finally, make the alert dialog using builder
                        val dialog: AlertDialog = alertDialogBuilder.create()

                        // Display the alert dialog on app interface
                        dialog.dismiss()

                        val intent = Intent(this, LoginActivity::class.java)
                        intent.putExtra(SessionManager.KEY_EMAIL, email)
                        startActivity(intent)
                        finish()
                    } else {

                        add_member.isEnabled = true
                        Log.d("text4", email)
                        Log.d("text6", editText6.text.toString())
                        var respuesta = ""

                        alertDialogBuilder.setTitle("Error registrando usuario")
                        if(task.exception!!.message!!.contains("The email address is already in use by another account"))

                        {
                            respuesta = "La dirección de correo electrónico ya esta en uso, por favor ingrese un correo distinto."
                        }
                        if(task.exception!!.message!!.contains("The given password is invalid"))

                        {
                            respuesta = "La contraseña no cumple con el tamaño requerido."
                        }
                        else
                        {
                            respuesta = task.exception!!.message!!
                        }
                        alertDialogBuilder.setMessage(respuesta)


                        // Display a neutral button on alert dialog
                        alertDialogBuilder.setNeutralButton("Entendido.") { _, _ ->
                            Toast.makeText(
                                applicationContext,
                                "You cancelled the dialog.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        // Finally, make the alert dialog using builder
                        val dialog: AlertDialog = alertDialogBuilder.create()

                        // Display the alert dialog on app interface
                        dialog.show()

                    }
                }
        } else {
            val snackbar = Snackbar.make(
                signuplayout, "No hay conexión a internet!",
                Snackbar.LENGTH_INDEFINITE
            ).setAction("Aceptar", View.OnClickListener { })
            snackbar.setActionTextColor(Color.WHITE)
            snackbar.show()
        }

    }

    fun getRandomString(length: Int) : String {
        val allowedChars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz"
        return (1..length)
            .map { allowedChars.random() }
            .joinToString("")
    }


    private fun writeNewUser(userNamee: String, email: String) {

        val members = HashMap<String, Any>()
        val inside = HashMap<String, Any>()
        val preferences = HashMap<String, Any>()
        val user = HashMap<String, Any>()
        val gusta = listOf<Any>()
        val nogusta = listOf<String>()
        val listamiembros = mutableListOf<Any>()
        val listaCompras =  listOf<String>()
        val productos = HashMap<String, Any>()
        var id = "No hay ningun miembro agregado!"
        members.put(id,inside)
        inside.put("id",id)
        inside.put("name", "No hay ningun miembro agregado!")
        inside.put("objective", "NONE")
        inside.put("preferences", preferences)

        productos.put("nombre", "")
        productos.put("cantidad", "")


        preferences.put("likes", gusta)
        preferences.put("dislikes", nogusta)

        listamiembros.add(members)

        user.put("budget",0)
        user.put("email", email)
        user.put("strata", 0)
        user.put("objective", "NONE")
        user.put("members", members)
        user.put("preferences", preferences)
        user.put("recipes", gusta)
        user.put("shoppinglist", listaCompras)
        user.put("name",userNamee)


        db.collection("users").document(email).set(user).addOnSuccessListener {
            toast("registado con exito!")
        }.addOnFailureListener()
        {
            toast("pues no, no funciona")
        }

    }
}
