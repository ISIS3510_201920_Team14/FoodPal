package com.team14.foodpal.ui.user

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import com.team14.foodpal.util.Member
import com.team14.foodpal.util.RealMember
import com.team14.foodpal.util.User

class DBAsyncTask<R, T>(
    private val args: R,
    private val run: (args: R) -> T,
    private val post: (res: T) -> Unit = {}
) :
    AsyncTask<Void, Void, T>() {
    override fun doInBackground(vararg p0: Void?): T {
        return run(args)
    }

    override fun onPostExecute(result: T) {
        post(result)
    }
}

class UserRepository constructor(
    private val db: FirebaseFirestore,
    private val dao: UserDao
) {

    fun getUser(email: String, onSuccess: (user: User?) -> Unit) {
        DBAsyncTask<String, Any>(email, { mail ->
            val userExists = dao.load(mail)

//            if (userExists != null && false) { //TODO
//                return@DBAsyncTask userExists
//            } else {
            return@DBAsyncTask db.collection(COLLECTION_USERS)
                // .document(mail)
                .whereEqualTo("email", mail)
                .get()
//            }
        }, {
            when (it) {
                is User -> {
                    onSuccess(it)
                }
                else -> {
                    (it as Task<*>)
                        .addOnSuccessListener { qs ->
                            Log.d("QUERY", (qs as QuerySnapshot).documents.toString())
                            val user = qs.documents[0].toObject(User::class.java)
                            user?.let {
                                DBAsyncTask(user, { u -> dao.save(u) }, {}).execute()
                            }
                            onSuccess(user)

                        }
                }
            }
        }).execute()
    }

    fun updateUser(user: User, onSuccess: () -> Unit) {
        DBAsyncTask<User, Task<*>>(user, {
            val task = db.collection(COLLECTION_USERS)
                .document(it.email)
                .update("name", it.name, "objective", it.objective.toStrDB())
            dao.update(it)

            return@DBAsyncTask task
        }, {
            it.addOnSuccessListener {
                onSuccess()
            }
        }).execute()
    }

    fun addMember(member: RealMember, email: String, onSuccess: (rMember: RealMember) -> Unit) {
        DBAsyncTask(member, {
            return@DBAsyncTask db.collection(COLLECTION_USERS)
                .document(email)
                .update("members.${it.id}", it)
        }, {
            it.addOnSuccessListener { onSuccess(member) }
        }).execute()
    }

    fun updateMember(member: Member, email: String, position: String, onSuccess: () -> Unit) {
        DBAsyncTask<Member, Task<*>>(member, {
            return@DBAsyncTask db.collection(COLLECTION_USERS)
                .document(email)
                .update(
                    "members.$position.name",
                    it.name,
                    "members.$position.objective",
                    it.objective.toStrDB()
                )
        }, {
            it.addOnSuccessListener {
                onSuccess()
            }
        }).execute()
    }

    fun savePreferencesUser(
        email: String,
        attribute: String,
        data: List<String>,
        onSuccess: () -> Unit
    ) {
        DBAsyncTask<List<String>, Task<*>>(data, {
            return@DBAsyncTask db.collection(COLLECTION_USERS)
                .document(email)
                .update("preferences.$attribute", it)
        }, {
            it.addOnSuccessListener { onSuccess() }
        }).execute()
    }

    fun savePreferencesMember(
        email: String,
        attribute: String,
        position: String,
        data: List<String>,
        onSuccess: () -> Unit
    ) {
        DBAsyncTask<List<String>, Task<*>>(data, {
            return@DBAsyncTask db.collection(COLLECTION_USERS)
                .document(email)
                .update("members.$position.preferences.$attribute", it)
        }, {
            it.addOnSuccessListener { onSuccess() }
        }).execute()
    }

    fun saveBudget(email: String, budget: Int, onError: () -> Unit = {}, onSuccess: () -> Unit) {
        DBAsyncTask<Int, Task<*>>(budget, {
            return@DBAsyncTask db.collection(COLLECTION_USERS)
                .document(email)
                .update("budget", it)
        }, {
            it.addOnSuccessListener { onSuccess() }
                .addOnFailureListener { onError() }
        }).execute()
    }

    fun getFood(onSuccess: (food: List<String>) -> Unit) {
        db.collection(COLLECTION_FOOD)
            .get()
            .addOnSuccessListener {
                val list = it.map { f -> f.get("name") as String }
                onSuccess(list)
            }
    }

    companion object {
        const val COLLECTION_USERS = "users"
        const val COLLECTION_FOOD = "food"

        @Volatile
        private var instance: UserRepository? = null

        fun getInstance(db: FirebaseFirestore, ctx: Context): UserRepository {
            return instance
                ?: synchronized(UserRepository::class) {
                    val room = AppDatabase.getInstance(ctx.applicationContext)
                    val userDao = room.userDao()
                    instance
                        ?: UserRepository(
                            db,
                            userDao
                        ).also { instance = it }
                }
        }
    }
}
