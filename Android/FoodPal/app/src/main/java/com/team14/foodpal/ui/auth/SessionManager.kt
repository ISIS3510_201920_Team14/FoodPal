package com.team14.foodpal.ui.auth

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences

public class SessionManager {
    lateinit var pref: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var con: Context
    var PRIVATE_MODE: Int=0
    constructor(con: Context) {
        this.con = con
        pref = con.getSharedPreferences(PREF_NAME,PRIVATE_MODE)
        editor = pref.edit()

    }


    companion object {
        val PREF_NAME: String ="nombre"
        val IS_LOGIN : String = "esta logeado"
        val KEY_NAME : String = "nombre"
        val KEY_EMAIL : String = "correo"
    }

    fun createLoginSession (name:String, email:String){
        editor.putBoolean(IS_LOGIN,true)
        editor.putString(KEY_NAME,name)
        editor.putString(KEY_EMAIL,email)
        editor.commit()
    }
    fun checklogin()
    {
        if (!this.isLoggedIn())
        {
            var i: Intent = Intent(con,ChooseActivity::class.java)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            con.startActivity(i)
        }
    }

    fun checkUserDetails(): HashMap<String, String>
    {
        var user:Map<String,String> = HashMap<String,String>()
        (user as HashMap).put(KEY_NAME, pref.getString(KEY_NAME,null)!!)
        (user as HashMap).put(KEY_EMAIL,pref.getString(KEY_EMAIL,null)!!)
        return user
    }

    fun logoutUser()
    {
        editor.clear()
        editor.commit()

        var i:Intent = Intent(con,ChooseActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        con.startActivity(i)
    }

    fun isLoggedIn(): Boolean {
        return pref.getBoolean(IS_LOGIN,false)
    }

}