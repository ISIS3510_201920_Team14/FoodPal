package com.team14.foodpal.ui.auth

import android.view.View
import androidx.lifecycle.ViewModel

class AuthViewModel : ViewModel() {

    var name: String? = null
    var email: String? = null
    var password: String? = null

    var authListener: AuthListener? = null

    fun onLoginButtonClick(View: View)
    {
        authListener?.onStarted()
        if(email.isNullOrEmpty() || password.isNullOrEmpty()){
            authListener?.onFailure("email o contraseña invalida")
            return
        }
        authListener?.onSuccess()
    }
    fun onSignInButtonClick(View: View)
    {
        authListener?.onStarted()
        if(email.isNullOrEmpty() || password.isNullOrEmpty() || name.isNullOrEmpty() ){
            authListener?.onFailure("nombre, correo electronico, o contraseña invalidas")
            return
        }
        authListener?.onSuccess()
    }



}

